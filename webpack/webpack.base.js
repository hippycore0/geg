const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const pkg = require('../package');

module.exports = (options) => ({
	mode: options.mode,
	devtool: options.devtool,
	entry: options.entry,
	output: Object.assign(
		{
			path: path.resolve(process.cwd(), 'build'),
			publicPath: `/`,
		},
		options.output,
	),
	optimization: options.optimization,
	module: {
		rules: [
			{
				test: /\.svg$/,
				use: ['@svgr/webpack'],
			},
			{
				test: /^(?!.*\.test\.js$).*\.(js|jsx)$/, // Transform all .js files required somewhere with Babel
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: ['file-loader'],
			},
		],
	},
	plugins: options.plugins.concat([
		new HtmlWebpackPlugin({
			hash: true,
			title: 'GEG Finance',
			template: './src/index.html',
			environment: options.mode,
		}),
		new FaviconsWebpackPlugin({
			logo: './assets/favicon.png',
			version: pkg.version,
			theme_color: "#578a69",
			icons: {
				android: false,
				appleIcon: false,
				appleStartup: false,
				coast: false,
				firefox: false,
				windows: false,
				yandex: false
			}
		}),
		new webpack.DefinePlugin({
			APP_VERSION: JSON.stringify(pkg.version),
			MODE: JSON.stringify(options.mode),
			API_URL: JSON.stringify(
				process.env.API_URL || 'https://app.geg.finance/api',
			),
		}),
	]),
});
