const path = require('path');
const webpack = require('webpack');
const S3Plugin = require('webpack-s3-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const pkg = require('../package');
const staticPath = 'geg.app';

module.exports = require('./webpack.base')({
	mode: 'production',
	devtool: 'source-map',
	entry: [path.join(process.cwd(), 'src/index.js')],
	output: {
		path: path.resolve(process.cwd(), 'build'),
		publicPath: `https://static.geg.finance/${staticPath}/${pkg.version}/`,
	},
	plugins: [
		// new BundleAnalyzerPlugin(),
		new S3Plugin({
			// Exclude uploading of html
			exclude: /.*\.html$/,
			// s3Options are required
			s3Options: {
				accessKeyId: process.env.AWS_ACCESS_KEY_ID,
				secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
				region: 'eu-central-1',
			},
			s3UploadOptions: {
				Bucket: 'geg.app',
			},
			basePath: `${staticPath}/${pkg.version}`,
			cloudfrontInvalidateOptions: {
				DistributionId: process.env.CLOUDFRONT_DISTRIBUTION_ID,
				Items: [`/${staticPath}/${pkg.version}/*`],
			},
		}),
		new CompressionPlugin({
			test: /\.js(\?.*)?$/i,
		}),
	],
});
