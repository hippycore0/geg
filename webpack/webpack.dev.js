const path = require('path');

module.exports = require('./webpack.base')({
	mode: 'development',
	devtool: 'eval-source-map',
	plugins: [],
	output: {
		path: path.resolve(process.cwd(), 'build'),
		publicPath: `/`,
	},
	devServer: {
		historyApiFallback: true,
		host: '192.168.1.71',
		hot: true,
	}
});
