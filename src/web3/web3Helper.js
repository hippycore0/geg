import web3, { getProvider } from './Web3Obj';
import oracleAbi from '../abi/oracle';
import erc20Helper from './erc20Helper';

export default {
	initProvider: () => {

	},
	makeDeposit: (params) => {
		if (params.token) {
			return erc20Helper.makeDeposit(params);
		}
		console.log('makeDeposit', params);
		const contract = web3.addContract(params.contractId);
		try {
			return contract.methods
				.deposit(params.autoRenewal)
				.send({ from: params.from, value: web3.utils.toWei(params.value, 'ether') })
				.on('transactionHash', params.onHash)
				.on('receipt', params.onReceipt)
				.on('confirmation', params.onConfirm)
				.on('error', params.onError);
		} catch (e) {
			console.error(e);
			// debugger;
			params.onError(e);
		}
	},
	makeWithdrawal: async (params) => {
		console.log('TOKEN WITHDRAWAL', params.token);
		if (!params.depositId) {
			throw new Error('No deposit ID!');
		}
		if (params.token) {
			return erc20Helper.makeWithdrawal(params);
		}

		const contract = web3.addContract(params.contractId);
		console.log('makeWithdrawal', params.contractId, params.from, params.depositId, contract);
		const res = await contract.methods
			.makeWithdrawal(params.depositId)
			.send({ from: params.from })
			.on('transactionHash', params.onHash)
			.on('receipt', params.onReceipt)
			.on('confirmation', params.onConfirm)
			.on('error', params.onError);
		console.log('makeWithdrawal success', res);

		return res;
	},
	makeClaim: async ({ contractId, depositId, from, onHash, onReceipt, onConfirm, onError }) => {
		const contract = web3.addContract(contractId);
		if (!depositId) {
			throw new Error('No deposit ID!');
		}
		console.log('accrueInterestOne', contractId, from, depositId, contract);
		const res = await contract.methods
			.accrueInterestOne(depositId)
			.send({ from })
			.on('transactionHash', onHash)
			.on('receipt', onReceipt)
			.on('confirmation', onConfirm)
			.on('error', onError);
		console.log('accrueInterestOne success', res);
		return res;
	},
	makeClaimWithRates: async ({ contractId, depositId, amount, from, timestamp, signature, onHash, onReceipt, onConfirm, onError }) => {
		const contract = web3.addContract(contractId);
		if (!depositId) {
			throw new Error('No deposit ID!');
		}
		console.log('accrueInterestOneWithRates', contractId, from, depositId, amount, timestamp, signature);
		const res = await contract.methods
			.accrueInterestOneWithRates(depositId, web3.utils.toWei(amount), timestamp, signature)
			.send({ from })
			.on('transactionHash', onHash)
			.on('receipt', onReceipt)
			.on('confirmation', onConfirm)
			.on('error', onError);
		console.log('accrueInterestOne success', res);
		return res;
	},
	getAccounts: async () => {
		const provider = getProvider();
		if (!provider) {
			console.error('No provider!');
			return [];
		}

		await provider.enable();
		const accounts = await web3.eth.getAccounts();
		const typeNetwork = await web3.eth.net.getNetworkType();
		console.log('NET:', typeNetwork);
		console.log('accounts:', accounts);
		return {accounts, network: typeNetwork};
	},
	getBalance: (account) => {
		return new Promise((resolve, reject) => {
			web3.eth.getBalance(account, function (error, result) {
				if (error) {
					reject(error);
				}
				console.log('balance', result);
				resolve(web3.utils.fromWei(result));
			});
		});
	},
	getDeposit: async (contractId, account, productId) => {
		const contract = web3.addContract(contractId);
		const res = await contract.getPastEvents('LogDeposit', { fromBlock: 0, filter: { _from: account } });
		// const res2 = await contract.methods.deposits(productId).call();
		// console.log(res2);
		// debugger;
		console.log('LogDeposit success', contractId, res);
		return res;
	},
	getWithdrawal: async (contractId, account) => {
		const contract = web3.addContract(contractId); //new web3.eth.Contract(contractAbi, contractId);//
		const res = await contract.getPastEvents('LogWithdrawal', { fromBlock: 0, filter: { _from: account } });
		console.log('LogWithdrawal success', contractId, res);
		return res;
	},
	getBlock: async (blockNumber) => {
		return await web3.eth.getBlock(blockNumber);
	},
	getClaims: async (contractId, account) => {
		const contract = web3.addContract(contractId);
		console.log('LogAccruedInterest', contractId);
		const res = await contract.getPastEvents('LogAccruedInterest', { fromBlock: 0, filter: { _from: account } });
		console.log('LogAccruedInterest success', res);
		return res;
	},
	setAutoRenewal: async ({ contractId, depositId, from, autoRenewal }) => {
		const contract = web3.addContract(contractId);
		console.log('setAutoRenewal', contractId, depositId, autoRenewal);
		try {
			const res = await contract.methods.setAutoRenewal(depositId, autoRenewal).send({ from });
			return res;
		} catch (e) {
			MODE === 'development' && console.error(e);
		}

	},
	getAutoRenewal: async ({ contractId, depositId, from }) => {
		const contract = web3.addContract(contractId);
		try {
			const res = await contract.getPastEvents('LogAutoRenewal', {
				fromBlock: 0,
				filter: { _id: depositId, _from: from }
			});
			console.log('getAutoRenewal', res);
			return res[res.length - 1].returnValues._isRenewal;
		} catch (e) {
			MODE === 'development' && console.error(e);
		}
	},
	oracleUpdate: async ({ from, oracleContractId, productTokenAddress }) => {
		const contract = web3.addContract(oracleContractId, oracleAbi);
		console.log('oracleContractId', oracleContractId);
		console.log('productTokenAddress', productTokenAddress);
		const res = await contract.methods.updated(productTokenAddress).call({ from });
		console.log('oracleUpdate', res);
		return res;
	},
};
