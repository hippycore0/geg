import Web3 from 'web3';
import contractAbi from '../abi/gETHabi';
// import WalletConnectProvider from "@walletconnect/web3-provider";

let { ethereum } = window;
// let provider = ethereum;

let provider;
if(ethereum) {
	provider = ethereum
} else {
	try {
		provider = Web3.givenProvider;
	} catch (e) {
		console.error(e);
	}
}
const web3 = new Web3(provider);
const contracts = {};
web3.contracts = contracts;
const addContract = (contract, abi) => {
	if (contracts.hasOwnProperty(contract)) {
		return contracts[contract];
	}
	contracts[contract] = new web3.eth.Contract(abi || contractAbi, contract);
	return contracts[contract];
};
web3.addContract = addContract;

export const getProvider = () => {
	return provider;
};

export default web3;
