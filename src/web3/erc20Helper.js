import web3 from './Web3Obj';
import erc20Abi from '../abi/erc20abi';
import gegErc20WrapAbi from '../abi/gegErc20WrapAbi';

const sendDeposit = ({
	contractProduct,
	weiValue,
	from,
	autoRenewal,
	onHash,
	changeHash,
	setMessage,
	onReceipt,
	onConfirm,
	onError,
	                     isAllowed
}) =>
	contractProduct.methods
		.deposit(weiValue, autoRenewal)
		.send({ from })
		.on('transactionHash', (hash) => {
			console.log('deposit hash', hash);
			if(isAllowed) {
				onHash(hash);
			} else {
				changeHash(hash);
				setMessage(
					'Waiting for confirmation of deposit transaction. \n' +
					'Open Metamask to speed up the transaction in case of need.',
				);
			}

		})
		.on('receipt', (receipt) => {
			console.log('deposit receipt', receipt);
			onReceipt(receipt);
			setMessage('Transaction completed.');
		})
		.on('confirmation', onConfirm)
		.on('error', (e) => {
			console.error(e);
			// debugger;
			onError(e);
		});

export default {
	makeDeposit: async ({
		contractId,
		from,
		value,
		token,
        autoRenewal,
		changeHash,
		setMessage,
		onHash,
		onReceipt,
		onConfirm,
		onError,
	}) => {
		const contractToken = new web3.eth.Contract(erc20Abi, token); //erc20Contracts[token];
		const contractProduct = new web3.eth.Contract(gegErc20WrapAbi, contractId); // web3.addContract(contractId, gegErc20WrapAbi);

		console.log('CONTRACT', contractId);
		console.log('PRODUCT TOKEN', token);
		console.log('FROM', from);

		const weiValue = web3.utils.toWei(value, 'ether');
		//console.log('weiValue', weiValue);
		//

		const isAllowed = await contractToken.methods.allowance(from, contractId).call();
		console.log('IS ALLOWED', isAllowed);

		if (parseInt(isAllowed) > 0) {
			return sendDeposit({
				contractProduct,
				weiValue,
				from,
				autoRenewal,
				onHash,
				changeHash,
				setMessage,
				onReceipt,
				onConfirm,
				onError,
				isAllowed: true,
			});
		}
		// setMessage('Step 1: Confirm the permission in Metamask wallet.');
		const approveAmount = new web3.utils.BN(
			'115792089237316195423570985008687907853269984665640564039457584007913129639935',
		);
		const res = await contractToken.methods
			.approve(contractId, approveAmount)
			.send({ from })
			.on('transactionHash', (tx) => {
				onHash(tx);
				setMessage('Waiting for confirmation of access from Metamask.');
			})
			.on('receipt', (receipt) => {
				console.log('approve receipt', receipt);
			})
			.on('confirmation', () => {
				console.log('approve confirmation');
			})
			.on('error', onError)
			.then((e) => {
				console.log('approve then', e);
				setMessage('Step 2: Deposit approved. Confirm deposit transaction in Metamask wallet.');
				return sendDeposit({
					contractProduct,
					weiValue,
					from,
					autoRenewal,
					onHash,
					changeHash,
					setMessage,
					onReceipt,
					onConfirm,
					onError,
					isAllowed: false,
				});
			});

		console.log('approve return', res);
		return res;
	},
	makeWithdrawal: async ({ contractId, from, value, depositId, onHash, onReceipt, onConfirm, onError }) => {
		const contractProduct = new web3.eth.Contract(gegErc20WrapAbi, contractId);
		// const weiValue = web3.utils.toWei(value.toString(), 'ether');
		const res = await contractProduct.methods
			.makeWithdrawal(depositId)
			.send({ from })
			.on('transactionHash', onHash)
			.on('receipt', onReceipt)
			.on('confirmation', onConfirm)
			.on('error', onError);
		return res;
	},
	getTokenBalance: async ({ account, token, symbol }) => {
		const tokenContract = new web3.eth.Contract(erc20Abi, token); // erc20Contracts[token]; //
		const result = await tokenContract.methods.balanceOf(account).call();
		console.log(`erc20 balance for ${symbol}`, result);
		return web3.utils.fromWei(result, 'ether');
	},
};
