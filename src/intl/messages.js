import ru from '../../langs/ru';
import de from '../../langs/de';
import ch from '../../langs/ch';
import es from '../../langs/es';

export default {
	ru,
	de,
	ch,
	es,
};
