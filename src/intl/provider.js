import React from 'react';
import { useSelector } from 'react-redux';
import { IntlProvider } from 'react-intl';
import messages from './messages';

const AppIntl = ({ children }) => {
	const { activeLang } = useSelector((state) => state.lang);
	const activeMessages = messages[activeLang] ? messages[activeLang] : [];
	// console.log(activeMessages);
	return (
		<IntlProvider messages={activeMessages} locale={activeLang} defaultLocale="en">
			{children}
		</IntlProvider>
	);
};

export default AppIntl;
