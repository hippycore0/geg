import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory, createHashHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import rootReducer from './reducers';
import rootSaga from './sagas';

export const history = MODE === 'development' ? createHashHistory() : createBrowserHistory();

/* eslint-disable no-underscore-dangle, indent */
const composeEnhancers =
	process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
		? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
		: compose;
/* eslint-enable */

const sagaMiddleware = createSagaMiddleware();

const enhancer = composeEnhancers(
	applyMiddleware(sagaMiddleware, routerMiddleware(history)),
	// other store enhancers if any
);

const store = (preState) => {
	const store = createStore(rootReducer(history), preState, enhancer);
	sagaMiddleware.run(rootSaga);
	return store;
};
export default store;
