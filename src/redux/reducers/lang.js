import { handleActions } from 'redux-actions';
import { getCookie } from '../../tools/cookieHelper';
import { Settings } from 'luxon';

const savedLang = getCookie('GegSavedLang');
Settings.defaultLocale = savedLang || 'en';

const initialState = {
	activeLang: savedLang || 'en',
};

export default handleActions(
	{
		SET_LANG: (state, { payload }) => ({ ...state, activeLang: payload }),
	},
	initialState,
);
