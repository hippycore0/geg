import { handleActions } from 'redux-actions';
import { loadingBehavior } from './common';
import web3 from '../../web3/Web3Obj';
const initialState = {
	loading: [],
	entries: [],
	lastUpdate: new Date(),
};

export default handleActions(
	{
		...loadingBehavior('claims'),
		SET_CLAIMS: (state, { payload }) => ({
			...state,
			lastUpdate: new Date(),
			entries: payload.map((entry) => ({
				transactionHash: entry.transactionHash,
				blockNumber: entry.blockNumber,
				contract: entry.address,
				from: entry.returnValues._from,
				id: entry.returnValues._id,
				value: parseFloat(web3.utils.fromWei(entry.returnValues._value, 'ether')),
			})),
		}),
	},
	initialState,
);
