import { setDeposit, setWalletDeposits } from '../deposits';

const state = {
	entries: [
		{
			id: 1,
			value: 0.3,
			timestamp: 0,
			contract: 'contract_hash_1',
			transactionHash: 'tx_hash_1',
		},
	],
};
test('setWalletDeposits', () => {
	const payload = [
		{
			product: 1,
			product_contract_id: 'contract_hash_1',
			blockchain_events: [
				{
					tx_hash: 'tx_hash_1',
					value: '0.4',
				},
				{
					tx_hash: 'tx_hash_2',
					value: '0.2',
				},
			],
		},
	];

	const result = {
		entries: [
			{
				id: 1,
				value: 0.3,
				contract: 'contract_hash_1',
				timestamp: 0,
				transactionHash: 'tx_hash_1',
			},
			{
				id: 1,
				value: 0.2,
				contract: 'contract_hash_1',
				timestamp: 0,
				transactionHash: 'tx_hash_2',
			},
		],
	};
	expect(setWalletDeposits(state, { payload })).toEqual(result);
});

test('setDeposit', () => {
	const payload = [
		{
			returnValues: { _id: 1, _value: 0.4 },
			timestamp: 0,
			address: 'contract_hash_1',
			transactionHash: 'tx_hash_1',
		},
		{
			returnValues: { _id: 1, _value: 0.2 },
			timestamp: 0,
			address: 'contract_hash_1',
			transactionHash: 'tx_hash_2',
		},
	];

	const result = {
		entries: [
			{
				id: 1,
				value: 0.4,
				timestamp: 0,
				contract: 'contract_hash_1',
				transactionHash: 'tx_hash_1',
			},
			{
				id: 1,
				value: 0.2,
				timestamp: 0,
				contract: 'contract_hash_1',
				transactionHash: 'tx_hash_2',
			},
		],
	};
	expect(setDeposit(state, { payload })).toEqual(result);
});
