import { handleActions } from 'redux-actions';
import { LOADING } from '../actionTypes';

// todo: make object
const initialState = [];

export default handleActions(
	{
		[LOADING.ADD]: (state, { payload }) => (state.indexOf(payload) < 0 ? state.concat(payload) : state),
		[LOADING.REMOVE]: (state, { payload }) => {
			const index = state.indexOf(payload);
			if (index < 0) {
				return state;
			}
			return [...state.slice(0, index), ...state.slice(index + 1)];
		},
	},
	initialState,
);
