import { handleActions } from 'redux-actions';
import { loadingBehavior } from './common';
import web3 from '../../web3/Web3Obj';

const initialState = {
	entries: [],
	loading: [],
	lastUpdate: new Date(),
};

export default handleActions(
	{
		...loadingBehavior('withdrawal'),
		SET_WITHDRAWAL: (state, { payload }) => ({
			...state,
			lastUpdate: new Date(),
			entries: payload
				.map((entry) => ({
					transactionHash: entry.transactionHash,
					blockNumber: entry.blockNumber,
					contract: entry.address,
					from: entry.returnValues._from,
					id: parseInt(entry.returnValues._id, 10),
					value: parseFloat(web3.utils.fromWei(entry.returnValues._value, 'ether')),
				}))
				.concat(
					state.entries.filter((entry) => !payload.find((p) => p.transactionHash === entry.transactionHash)),
				),
		}),
	},
	initialState,
);
