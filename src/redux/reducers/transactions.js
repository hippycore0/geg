import { handleActions } from 'redux-actions';

const initialState = [];
/*
blockHash: "0x62c7d38b91f00614f1f31b9548f88e6a7db5ce01b6d2c60bf33b9279a0d9e4b4"
blockNumber: 9283465
contractAddress: null
cumulativeGasUsed: 1680640
from: "0x8b7c73460431c47b4bfc0d7a9d16a452a9a79748"
gasUsed: 144660
logs: (2) [{…}, {…}]
logsBloom: "0x0400000000000000..."
status: true
to: "0xa4586bea4af80436f62014dfc9b220e64694c48c"
transactionHash: "0x68ea3c217e0a29eca6667d5d19c7b2a62d10be9d89a7115a3b6e3df7b80439d2"
transactionIndex: 26
 */
export default handleActions(
	{
		SET_TRANSACTION: (state, { payload: { hash, type, ...tx } }) => {
			const index = state.findIndex((t) => t.transactionHash === hash);
			if (index >= 0) {
				return state;
			}
			return state.concat({
				type,
				...tx,
				transactionHash: hash,
				confirmed: false,
			});
		},
		SET_TRANSACTION_STATUS: (state, { payload: { hash, ...payload } }) => {
			const index = state.findIndex((t) => t.transactionHash === hash);
			if (index < 0) {
				return state;
			}
			return state.map((transaction) => {
				if (transaction.transactionHash !== hash) {
					return transaction;
				}
				return {
					...transaction,
					...payload,
				};
			});
		},
		CHANGE_TRANSACTION_HASH: (state, { payload: { oldHash, newHash } }) => {
			const index = state.findIndex((t) => t.transactionHash === oldHash);
			if (index < 0) {
				return state;
			}
			return state.map((tx) => {
				if (tx.transactionHash === oldHash) {
					return {
						...tx,
						transactionHash: newHash,
					};
				}
				return tx;
			});
		},
		SET_TRANSACTION_ERROR: (state, { payload: { hash, error } }) => {
			const index = state.findIndex((t) => t.transactionHash === hash);
			if (index < 0) {
				return state;
			}
			return state.map((t) => {
				if (t.transactionHash !== hash) {
					return t;
				}
				return {
					...t,
					error,
				};
			});
		},
		SET_TRANSACTION_MESSAGE: (state, { payload: { hash, message } }) => {
			const index = state.findIndex((t) => t.transactionHash === hash);
			if (index < 0) {
				return state;
			}
			return state.map((transaction) => {
				if (transaction.transactionHash !== hash) {
					return transaction;
				}
				return {
					...transaction,
					message: message,
				};
			});
		},
		SET_TRANSACTION_CONFIRMED: (state, { payload }) => {
			const index = state.findIndex((t) => t.transactionHash === payload);
			if (index < 0) {
				return state;
			}
			return state.map((transaction) => {
				if (transaction.transactionHash !== payload) {
					return transaction;
				}
				return {
					...transaction,
					confirmed: true,
				};
			});
		},
	},
	initialState,
);
