import { handleActions } from 'redux-actions';
const initialState = {
	account: null,
	balance: 0,
	network: 'main',
	transactions: [],
	tokenBalance: {},
};

export default handleActions(
	{
		SET_ACCOUNT: (state, { payload }) => ({ ...state, account: payload }),
		SET_BALANCE: (state, { payload }) => ({ ...state, balance: payload }),
		SET_NETWORK: (state, { payload }) => ({ ...state, network: payload }),
		SET_TOKEN_BALANCE: (state, { payload: { token, value } }) => ({
			...state,
			tokenBalance: { ...state.tokenBalance, [token]: value },
		}),
	},
	initialState,
);
