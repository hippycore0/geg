import { handleActions } from 'redux-actions';

/*
{
        "id": 1,
        "name": "FAQ Home",
        "tag": "Home",
        "questions": [
            {
                "id": 1,
                "question": "Do you have any sum limits in token sale?",
                "answer": "Yes. The private sale stage has a minimum amount limit of 10,000 USDT and no maximum limit. The public stage has a minimum amount of 100 USDT and a maximum limit of 100,000 USDT.",
                "sort_order": 0,
                "topic": 1
            },
            {
                "id": 2,
                "question": "Are there any discounts on token sale?",
                "answer": "Yes, “early-bird” discounts are reserved for the first investors.",
                "sort_order": 0,
                "topic": 1
            }
        ]
    },
 */
const initialState = {
	lastUpdate: null,
	entries: [],
};

export default handleActions(
	{
		SET_FAQ: (state, { payload }) => ({ ...state, entries: payload, lastUpdate: new Date() }),
	},
	initialState,
);
