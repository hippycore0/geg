import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

// rig
import loading from './loading';
import errors from './errors';

// web3
import wallet from './wallet';
import deposits from './deposits';
import withdrawal from './withdrawal';
import transactions from './transactions';
import claims from './claims';

// api
import exchanges from './exchanges';
import products from './products';
import tokens from './tokens';
import oracle from './oracle';
import faq from './faq';

// other
import auth from './auth';
import lang from './lang';

export default (history) =>
	combineReducers({
		errors,
		loading,
		wallet,
		deposits,
		withdrawal,
		transactions,
		products,
		exchanges,
		tokens,
		oracle,
		faq,
		auth,
		claims,
		lang,
		router: connectRouter(history),
	});
