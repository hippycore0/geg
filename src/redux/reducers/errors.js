import { handleActions } from 'redux-actions';
import { ERROR } from '../actionTypes';

const initialState = [];

export default handleActions(
	{
		[ERROR.ADD]: (state, { payload }) => {
			const index = state.findIndex((e) => e.name === payload.name);
			if (index >= 0) {
				return state;
			}
			return state.concat(payload);
		},
		[ERROR.REMOVE]: (state, { payload }) => {
			const index = state.findIndex((e) => e.name === payload.name);
			if (index < 0) {
				return state;
			}
			return [...state.slice(0, index), ...state.slice(index + 1)];
		},
	},
	initialState,
);
