import { handleActions } from 'redux-actions';

const initialState = {
	isAuth: false,
};

export default handleActions(
	{
		SET_IS_AUTH: (state, { payload }) => ({ ...state, isAuth: payload }),
	},
	initialState,
);
