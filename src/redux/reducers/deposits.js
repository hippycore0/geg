import { handleActions } from 'redux-actions';
import { loadingBehavior } from './common';
import web3 from '../../web3/Web3Obj';

const initialState = {
	entries: [],
	loading: [],
	lastUpdate: new Date(),
	depositsAmount: [],
	totals: {},
	autoRenewalLoaders: [],
};

export const setDeposit = (state, { payload: { entries, erc20 } }) => {
	//debugger;
	return {
		...state,
		lastUpdate: new Date(),
		entries: state.entries
			.map((entry) => {
				const relEntry = entries.find(
					(e) =>
						parseInt(e.returnValues._id, 10) === entry.deposit_id &&
						entry.contract.toLowerCase() === e.address.toLowerCase(),
				);

				if (!relEntry) {
					return entry;
				}
				return {
					...entry,
					transactionHash: relEntry.transactionHash,
					timestamp: relEntry.timestamp,
					contract: relEntry.address,
					deposit_id: parseInt(relEntry.returnValues._id, 10),
					// value: entry.returnValues._value, // for tests
					value: web3.utils.fromWei(relEntry.returnValues._value, 'ether'),
				};
			})
			.concat(
				entries
					.filter(
						(e) =>
							!state.entries.find(
								(se) =>
									parseInt(e.returnValues._id, 10) === se.deposit_id &&
									se.contract.toLowerCase() === e.address.toLowerCase(),
							),
					)
					.map((relEntry) => ({
						transactionHash: relEntry.transactionHash,
						timestamp: relEntry.timestamp,
						contract: relEntry.address,
						deposit_id: parseInt(relEntry.returnValues._id, 10),
						// value: entry.returnValues._value, // for tests
						value: web3.utils.fromWei(relEntry.returnValues._value, 'ether'),
					})),
			),
	};
};

export const setWalletDeposits = (state, { payload }) => {
	const payloadDeposits = payload.reduce(
		(acc, val) =>
			acc.concat(val.deposits.map((d) => ({ ...d, product_id: val.product, contract: val.product_contract_id }))),
		[],
	);

	//debugger;
	return {
		...state,
		entries: state.entries
			.map((depositEntry) => {
				const relDeposit = payloadDeposits.find(
					(e) =>
						e.deposit_id === depositEntry.deposit_id &&
						depositEntry.contract.toLowerCase() === e.contract.toLowerCase(),
				);
				if (!relDeposit) {
					return depositEntry;
				}
				console.log(relDeposit);
				return {
					...depositEntry,
					deposit_id: relDeposit.deposit_id,
					contract: relDeposit.contract,
					product_id: relDeposit.product_id,
					interestToClaim: relDeposit.interest_to_claim,
					interestToClaimGeg: relDeposit.interest_to_claim_geg,
				};
			})
			.concat(
				payloadDeposits
					.filter(
						(e) =>
							!state.entries.find(
								(se) =>
									e.deposit_id === se.deposit_id &&
									se.contract.toLowerCase() === e.contract.toLowerCase(),
							),
					)
					.map((e) => ({
						autoRenewal: e.auto_renew,
						transactionHash: e.tx_hash,
						value: e.value,
						deposit_id: e.deposit_id,
						contract: e.contract,
						product_id: e.product_id,
						interestToClaim: e.interest_to_claim,
						interestToClaimGeg: e.interest_to_claim_geg,
						timestamp: e.timestamp,
					})),
			),
	};
};

export default handleActions(
	{
		...loadingBehavior('deposits'),
		SET_DEPOSIT: setDeposit,
		SET_WALLET_DEPOSITS: setWalletDeposits,
		SET_PRODUCTS_DEPOSIT: (state, { payload }) => {
			if (!payload) {
				return state;
			}
			return {
				...state,
				depositsAmount: payload,
			};
		},
		SET_WALLET_TOTALS: (state, { payload }) => {
			if (!payload) {
				return state;
			}
			return {
				...state,
				totals: payload,
			};
		},
		SET_DEPOSIT_AUTORENEWAL: (state, { payload: { depositId, contractId, value } }) => {
			if (!depositId) {
				return state;
			}
			return {
				...state,
				entries: state.entries.map((e) => {
					if (!(e.deposit_id === depositId && e.contract.toLowerCase() === contractId.toLowerCase())) {
						return e;
					}
					return { ...e, autoRenewal: value };
				}),
			};
		},
		SET_RENEWAL_LOADER: (state, { payload: { contractId, depositId } }) => {
			if (!depositId) {
				return state;
			}
			return {
				...state,
				autoRenewalLoaders: state.autoRenewalLoaders.concat(`${contractId}_${depositId}`),
			};
		},
		UNSET_RENEWAL_LOADER: (state, { payload: { depositId, contractId } }) => {
			if (!depositId) {
				return state;
			}
			const index = state.autoRenewalLoaders.indexOf(`${contractId}_${depositId}`);
			if (index < 0) {
				console.error(`deposit ${depositId} for contract ${contractId} not found in loaders`);
				return state;
			}
			return {
				...state,
				autoRenewalLoaders: [
					...state.autoRenewalLoaders.slice(0, index),
					...state.autoRenewalLoaders.slice(index + 1),
				],
			};
		},
		INCREASE_ALLOCATED_VALUE: (state, {payload: {gegValue, usdtValue}}) => {
			return {
				...state,
				totals: {
					...state.totals,
					total_deposited_geg: parseFloat(state.total_deposited_geg) + parseFloat(gegValue),
					total_deposited_usdt: parseFloat(state.total_deposited_usdt) + parseFloat(usdtValue),
				}
			}
		},
		DECREASE_ALLOCATED_VALUE: (state, {payload: {gegValue, usdtValue}}) => {
			return {
				...state,
				totals: {
					...state.totals,
					total_deposited_geg: parseFloat(state.total_deposited_geg) - parseFloat(gegValue),
					total_deposited_usdt: parseFloat(state.total_deposited_usdt) - parseFloat(usdtValue),
				}
			}
		},
		DECREASE_INTERESTS_VALUE: (state, {payload: {gegValue, usdtValue}}) => {
			return {
				...state,
				totals: {
					...state.totals,
					total_interest_geg: parseFloat(state.total_interest_geg) - parseFloat(gegValue),
					total_interest_usdt: parseFloat(state.total_interest_usdt) - parseFloat(usdtValue),
				}
			}
		},
	},
	initialState,
);
