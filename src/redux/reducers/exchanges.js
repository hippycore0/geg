import { handleActions } from 'redux-actions';

/*
  {
        "id": 2,
        "name": "Binance",
        "logo": "http://production-alb-1702431244.eu-central-1.elb.amazonaws.com/media/exchange_logo/binance.png",
        "url": "https://www.binance.com/en/trade/UNI_USDT",
        "api_url": "",
        "is_active": true
    },
*/
const initialState = {
	lastUpdate: null,
	entries: [],
	rates: [],
};

export default handleActions(
	{
		SET_EXCHANGES: (state, { payload }) => ({ ...state, entries: payload, lastUpdate: new Date() }),
		SET_RATES: (state, { payload }) => ({ ...state, rates: payload }),
	},
	initialState,
);
