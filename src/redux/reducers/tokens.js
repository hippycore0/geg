import { handleActions } from 'redux-actions';

/*
   {
        "symbol": "USDT",
        "name": "USDT",
        "logo": "http://production-alb-1702431244.eu-central-1.elb.amazonaws.com/media/token_logo/usdt.png",
        "contract": "https://etherscan.io/token/0xdac17f958d2ee523a2206206994597c13d831ec7",
        "stable": true,
        "color": "#000000"
    },
*/
const initialState = {
	lastUpdate: null,
	entries: [],
	signatures: {},
};

export default handleActions(
	{
		SET_TOKENS: (state, { payload }) => ({ ...state, entries: payload, lastUpdate: new Date() }),
		SET_TOKEN_SIGNATURE: (state, {payload: {symbol, data}}) => ({...state, signatures: {...state.signatures, [symbol]: data }}),
	},
	initialState,
);
