export const deleteByFromState = (state, { payload }) => {
	const index = state.indexOf(payload);
	if (index < 0) {
		return state;
	}
	return [...state.slice(0, index), ...state.slice(index + 1)];
};

export const loadingBehavior = (name) => ({
	[`SET_${name.toUpperCase()}_LOADING`]: (state, { payload }) =>
		state.loading.indexOf(payload) >= 0 ? state : { ...state, loading: state.loading.concat(payload) },
	[`SET_${name.toUpperCase()}_UNLOADING`]: (state, { payload }) => ({
		...state,
		loading: deleteByFromState(state.loading, { payload }),
	}),
});
