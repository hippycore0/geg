import { handleActions } from 'redux-actions';
/*
 {
        "id": 2,
        "name": "USDT",
        "description": "deposit account, daily accrual, 1 month lock for deposit body, release of accumulated interest in 1 month",
        "contract_id": "",
        "contract_url": "",
        "apy": "7.00",
        "days": 30,
        "pool_volume": 1200000,
        "pool_volume_token": 0,
        "min_locked_amount": 0,
        "is_active": true,
        "is_default": false,
        "start_date": "2020-12-10T15:48:43Z",
        "end_date": "2020-12-31T12:00:00Z",
        "token_product": "USDT",
        "token_reward": "GEG"
    },
* */
const initialState = {
	lastUpdate: null,
	entries: [],
	walletDeposit: [],
	filter: 'All',
	totals: {
		max_volume_usdt: 0,
		value_allocated_usdt: 0,
		open_volume_usdt: 0,
	},
};

export default handleActions(
	{
		SET_PRODUCTS: (state, { payload }) => ({
			...state,
			entries: payload.filter((p) => p.is_active).map((p) => ({ ...p, amount: 0, progress: 0 })),
			lastUpdate: new Date(),
		}),
		SET_ALLOWED_PRODUCTS: (state, { payload }) => ({
			...state,
			entries: state.entries.concat(payload.filter(p => p.is_active).map(p => ({...p, amount: 0, progress: 0 })))
		}),
		SET_PRODUCTS_DEPOSIT: (state, { payload }) => ({
			...state,
			entries: state.entries.map((product) => {
				const deposit = payload.find((d) => d.product === product.id);
				if (!deposit) {
					return product;
				}
				const amount = parseFloat(deposit.amount);
				const progress = Number(((amount / product.pool_volume_token) * 100).toFixed(3));
				return {
					...product,
					progress,
					amount,
				};
			}),
		}),
		SET_PRODUCTS_FILTER: (state, { payload }) => ({ ...state, filter: payload }),
		SET_PRODUCTS_TOTALS: (state, { payload }) => ({ ...state, totals: payload }),
	},
	initialState,
);
