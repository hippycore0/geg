import { handleActions } from 'redux-actions';
import { loadingBehavior } from "./common";

const initialState = {
	entries: [],
	loading: [],
	oracleLastUpdate: null,
};

export default handleActions(
	{
		...loadingBehavior('oracle'),
		SET_ORACLE: (state, { payload }) => {
			return {
				...state,
				entries: payload,
			}
		},
		SET_ORACLE_LAST_UPDATE: (state, { payload }) => ({ ...state, oracleLastUpdate: payload }),
	},
	initialState,
);
