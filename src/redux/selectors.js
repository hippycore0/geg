export const DepositPeriods = {
	1: '1D+',
	30: '1M',
	60: '2M',
	90: '3M',
	91: '3M',
	182: '6M',
	273: '9M',
	365: '1Y',
	547: '1,5Y',
	730: '2Y',
};

export const PayoutIntervals = {
	0: 'anytime',
	1: 'everyday',
	7: '1 week',
	30: '1 month',
	90: '1 quarter',
};

export const ProductListSelector = (state, filterByNet = true) => {
	const { products, tokens, deposits, withdrawal, claims, wallet: { network } } = state;
	let processedProducts = products.entries;
	if(filterByNet) {
		processedProducts = products.entries
			.filter(product => {
				const relToken = tokens.entries.find((t) => t.uuid === product.token_product);
				return relToken && relToken.network.code === network;
			});
	}
	// console.log(processedProducts, filterByNet);
	const result = processedProducts
		.map((product) => {
			const relToken = tokens.entries.find((t) => t.uuid === product.token_product);
			const rewardToken = tokens.entries.find((t) => t.uuid === product.token_reward);
			const productDeposits = deposits.entries
				.filter(
					(deposit) =>
						deposit.contract.toLowerCase() === product.contract_id.toLowerCase() &&
						!withdrawal.entries.find(
							(w) =>
								w.id === deposit.deposit_id &&
								w.contract.toLowerCase() === deposit.contract.toLowerCase(),
						),
				)
				.map((deposit) => ({
					...deposit,
					claim: claims.entries
						.filter((claim) => claim.contract === deposit.contract && claim.id === deposit.id)
						.reduce((acc, claim) => acc + parseFloat(claim.value), 0),
				}))
				.sort((a, b) => b.timestamp - a.timestamp);

			const productDepositsAmount = deposits.depositsAmount.find((da) => da.product === product.id);

			return {
				...product,
				period: DepositPeriods[product.days] || '1D+',
				payout_interval_formatted: PayoutIntervals[product.payout_interval],
				erc20token: relToken ? relToken.erc20token : false,
				logo: relToken ? relToken.logo : null,
				rewardLogo: rewardToken ? rewardToken.logo : null,
				token_product_symbol: relToken ? relToken.symbol : null,
				token_product_contract: relToken ? relToken.contract_id : null,
				token_reward_symbol: rewardToken ? rewardToken.symbol : null,
				token_reward_contract: rewardToken ? rewardToken.contract_id : null,
				contractUrl:
					relToken && product.contract_id
						? `${relToken.network.url_explorer}token/${product.contract_id}`
						: null,
				deposits: productDeposits,
				progress: productDepositsAmount ? productDepositsAmount.percentage_filled : 0,
				tokensLeft: productDepositsAmount ? productDepositsAmount.tokens_left : null,
			};
		})
		.sort((a, b) => a.days - b.days);
	// console.log(result);
	return result;
};

export const isLoading = (state, { id, name }) =>
	state[name] && state[name].loading && state[name].loading.indexOf(id) >= 0;

export const ProductsForGetDeposit = (state) => {
	return state.products.entries
		.filter((p) => Boolean(p.contract_id) && p.is_active)
		.map((p) => ({
			contractId: p.contract_id,
			productId: p.id,
			erc20: state.tokens.entries.find((t) => t.uuid === p.token_product).erc20token,
		}));
};
