import { channel } from 'redux-saga';
import { all, takeLatest, takeEvery, select, take, call, put, race, delay } from 'redux-saga/effects';
import { DateTime } from 'luxon';
import web3Helper from '../../web3/web3Helper';
import { setCookie } from '../../tools/cookieHelper';
import { LOADING, ERROR } from '../actionTypes';
import { ProductsForGetDeposit } from '../selectors';
import Erc20Web3Helper from '../../web3/erc20Helper';
import apiRequest from "../../tools/apiRequest";

const transactionChannel = channel();
export const watchTransactionChannel = function* () {
	while (true) {
		const action = yield take(transactionChannel);
		yield put(action);
	}
};

const transactionListener = ({ type, depositId, productId }) => {
	let _hash = null;
	return {
		onHash: (hash) => {
			_hash = hash;
			return transactionChannel.put({ type: 'SET_TRANSACTION', payload: { hash, type, productId, depositId } });
		},
		changeHash: (newHash) => {
			const oldHash = _hash;
			_hash = newHash;
			return transactionChannel.put({ type: 'CHANGE_TRANSACTION_HASH', payload: { oldHash, newHash } });
		},
		onReceipt: (receipt) =>
			transactionChannel.put({ type: 'SET_TRANSACTION_STATUS', payload: { hash: _hash, receipt } }),
		onConfirm: (confirmationNumber) =>
			transactionChannel.put({
				type: 'SET_TRANSACTION_STATUS',
				payload: { hash: _hash, confirmationNumber },
			}),
		onError: (error) => transactionChannel.put({ type: 'SET_TRANSACTION_ERROR', payload: { hash: _hash, error } }),
		setMessage: (message) =>
			transactionChannel.put({
				type: 'SET_TRANSACTION_MESSAGE',
				payload: { hash: _hash, message },
			}),
	};
};

const doMakeMethod = (type, method, onSuccess) =>
	function* ({ payload }) {
		console.log('doMakeMethod', payload);
		const { contractId, from, value, depositId, productId, token, autoRenewal = false } = payload;
		yield put({ type: ERROR.REMOVE, payload: `MAKE_${type.toUpperCase()}` });
		yield put({ type: LOADING.ADD, payload: `MAKE_${type.toUpperCase()}` });
		yield put({ type: `SET_${type.toUpperCase()}_LOADING`, payload: depositId });
		try {
			yield call(method, {
				...payload,
				contractId,
				from,
				value,
				depositId,
				token,
				autoRenewal,
				...transactionListener({ type, productId, depositId }),
			});
			yield onSuccess(payload);
		} catch (e) {
			MODE === 'development' && console.error(e);
			yield put({ type: ERROR.ADD, payload: { name: `MAKE_${type.toUpperCase()}`, value: e } });
		}
		yield put({ type: LOADING.REMOVE, payload: `MAKE_${type.toUpperCase()}` });
		yield put({ type: `SET_${type.toUpperCase()}_UNLOADING`, payload: depositId });


	};


export function* Web3Sagas() {
	yield takeLatest(
		'MAKE_DEPOSIT',
		doMakeMethod('deposit', web3Helper.makeDeposit, function* ({ depositId, autoRenewal, value, contractId, from }) {
			yield put({ type: 'GET_DEPOSIT', payload: { contractId } });
			yield put({ type: 'GET_BALANCE', payload: from });
			yield put({ type: 'SET_DEPOSIT_AUTORENEWAL', payload: { depositId, contractId, value: autoRenewal } });
			const rates = yield select((state) => state.exchanges.rates);
			const gegValue = rates.find(r => r.from_token === token && r.to_token === 'GEG').value * value;
			const usdtValue = rates.find(r => r.from_token === token && r.to_token === 'USDT').value  * value;
			yield put({ type: 'INCREASE_ALLOCATED_VALUE', payload: { gegValue, usdtValue } });
		}),
	);

	yield takeLatest(
		'MAKE_WITHDRAWAL',
		doMakeMethod('withdrawal', web3Helper.makeWithdrawal, function* ({ contractId, depositId, from }) {
			/* UPDATE Finance common params on success */
			const deposit = yield select((state) => state.deposits.entries.find(d => d.deposit_id === depositId));
			const relProduct = yield select((state) => state.products.entries.find(p => p.id === deposit.product_id));
			const relToken = yield select((state) => state.tokens.entries.find(t => t.uuid === relProduct.token_product));
			const rates = yield select((state) => state.exchanges.rates);
			const gegRate = rates.find(r => r.from_token === relToken.symbol && r.to_token === 'GEG').value;
			const usdtRate = rates.find(r => r.from_token === relToken.symbol && r.to_token === 'USDT').value;

			yield put({ type: 'DECREASE_ALLOCATED_VALUE', payload: { gegValue: gegRate * deposit.value, usdtValue: usdtRate * deposit.value } });
			yield put({ type: 'DECREASE_INTERESTS_VALUE', payload: { gegValue: gegRate * deposit.interestToClaimGeg, usdtValue: usdtRate * deposit.interestToClaim } });


			yield put({ type: 'GET_WITHDRAWAL', payload: { contractId } });
			yield put({ type: 'GET_DEPOSIT', payload: { contractId } });
			yield put({ type: 'GET_BALANCE', payload: from });
			yield put({ type: 'FETCH_PRODUCTS_DEPOSIT' });
			yield put({ type: 'FETCH_PRODUCTS_TOTALS' });

		}),
	);

	yield takeLatest(
		'MAKE_CLAIM',
		function *({ payload }) {
			console.log('MAKE_CLAIM', payload);
			yield put({ type: ERROR.REMOVE, payload: `MAKE_CLAIM` });
			yield put({ type: LOADING.ADD, payload: `MAKE_CLAIM` });
			yield put({ type: `SET_CLAIMS_LOADING`, payload: payload.depositId });
			try {
				const activeNetwork = yield select(state => state.wallet.network);
				const oracleEntry = yield select(state => state.oracle.entries.find(e => e.network.code === activeNetwork));
				const deposit = yield select((state) => state.deposits.entries.find(d => d.deposit_id === payload.depositId));
				const relProduct = yield select((state) => state.products.entries.find(p => p.contract_id === payload.contractId));
				const relToken = yield select((state) => state.tokens.entries.find(t => t.uuid === relProduct.token_product));
				const account = yield select((state) => state.wallet.account);
				const rates = yield select((state) => state.exchanges.rates);
				const gegRate = relToken.symbol === 'GRG' ? 1 : rates.find(r => r.from_token === relToken.symbol && r.to_token === 'GRG').value;
				const usdtRate = rates.find(r => r.from_token === relToken.symbol && r.to_token === 'USDT').value;

				if(relToken.symbol === 'GRG') {
					yield call(web3Helper.makeClaim, {
						contractId: payload.contractId,
						depositId: payload.depositId,
						from: account,
						...transactionListener({ type: 'claim', depositId: payload.depositId, productId: relProduct.id })
					});
				} else {
					// проверяем в `/api/exchange-rates/` наличие курса {from_token: product.product_token.symbol, to_token: "GEG"}
					const relRate = yield select(state => {
						const res = state.exchanges.rates.find(r => r.from_token === relToken.symbol && r.to_token === 'GRG');
						if(!res) {
							debugger;
						}
						return res;
					});
					// Дергаем Оракул методом `updated(address)` , где address - это product.product_token.address
					// В ответ прилетает uint256 с таймштампом в формате unix epoch в секундах
					const timestamp = yield call(web3Helper.oracleUpdate, {
						from: account,
						oracleContractId: oracleEntry.address,
						productTokenAddress: relToken.contract_id
					});
					const rateModifyTimestamp = DateTime.fromISO(relRate.last_modified).toSeconds();
					// Если last_modified(п.4) на 60*30 (30 минут) больше, чем timestamp
					const diff = rateModifyTimestamp - timestamp > 60 * 30;
					console.log('diff', diff);

					if (diff) {
						// получаем из `/api/signature/${product.product_token.symbol}` данные для обновления курса валют: курс обмена, timestamp и цифровую подпись.
						const res = yield call(apiRequest, { url: `/signature/${relToken.symbol}`, method: 'GET' });
						// 	{"rate": "1.000000000000000000", "ts": 1616684951, "sig": ""}
						console.log('signature', res);
						yield call(web3Helper.makeClaimWithRates, {
							contractId: payload.contractId,
							depositId: payload.depositId,
							from: account,
							amount: res.rate,
							timestamp: res.ts,
							signature: res.sig,
							...transactionListener({ type: 'claim', depositId: payload.depositId, productId: relProduct.id })
						});
					} else {
						yield call(web3Helper.makeClaim, {
							contractId: payload.contractId,
							depositId: payload.depositId,
							from: account,
							...transactionListener({ type: 'claim', depositId: payload.depositId, productId: relProduct.id })
						});
					}
				}


				/* UPDATE Finance common params on success */
				yield put({
					type: 'DECREASE_INTERESTS_VALUE',
					payload: {
						gegValue: gegRate * deposit.interestToClaimGeg,
						usdtValue: usdtRate * deposit.interestToClaim
					}
				});
				yield put({ type: 'GET_CLAIM', payload: { contractId: payload.contractId } });
				yield put({ type: 'GET_DEPOSIT', payload: { contractId: payload.contractId } });
				yield put({ type: 'GET_BALANCE', payload: account });
				yield put({ type: 'FETCH_PRODUCTS_DEPOSIT' });
				yield put({ type: 'FETCH_PRODUCTS_TOTALS' });
			} catch (e) {
				MODE === 'development' && console.error(e);
				yield put({ type: ERROR.ADD, payload: { name: `MAKE_CLAIM`, value: e } });
			}
			yield put({ type: LOADING.REMOVE, payload: `MAKE_CLAIM` });
			yield put({ type: `SET_CLAIMS_UNLOADING`, payload: payload.depositId });
		}
	);

	yield takeEvery('GET_DEPOSIT', function* ({ payload: { contractId, productId, erc20 } }) {
		const account = yield select((state) => state.wallet.account);
		if (!account) {
			MODE === 'development' && console.warn('No account to GET_DEPOSIT');
		} else {
			yield put({ type: ERROR.REMOVE, payload: 'GET_DEPOSIT' });
			yield put({ type: LOADING.ADD, payload: 'GET_DEPOSIT' });
			try {
				const deposits = yield call(web3Helper.getDeposit, contractId, account, productId);
				if (deposits.length) {
					const blocks = yield all(deposits.map((entry) => web3Helper.getBlock(entry.blockNumber)));

					const timedDeposits = deposits.map((deposit) => ({
						...deposit,
						timestamp: blocks.find((block) => block.number === deposit.blockNumber).timestamp,
					}));
					yield put({ type: 'SET_DEPOSIT', payload: { entries: timedDeposits, erc20 } });
				}
			} catch (e) {
				MODE === 'development' && console.error(e);
				yield put({ type: ERROR.ADD, payload: { name: 'GET_DEPOSIT', value: e } });
			}
			yield put({ type: LOADING.REMOVE, payload: 'GET_DEPOSIT' });
		}
	});

	yield takeEvery('GET_WITHDRAWAL', function* ({ payload: { contractId } }) {
		const account = yield select((state) => state.wallet.account);
		if (!account) {
			MODE === 'development' && console.warn('No account to GET_WITHDRAWAL');
		} else {
			yield put({ type: ERROR.REMOVE, payload: 'GET_WITHDRAWAL' });
			yield put({ type: LOADING.ADD, payload: 'GET_WITHDRAWAL' });
			try {
				const result = yield call(web3Helper.getWithdrawal, contractId, account);
				yield put({ type: 'SET_WITHDRAWAL', payload: result });
			} catch (e) {
				MODE === 'development' && console.error(e);
				yield put({ type: ERROR.ADD, payload: { name: 'GET_WITHDRAWAL', value: e } });
			}
			yield put({ type: LOADING.REMOVE, payload: 'GET_WITHDRAWAL' });
		}
	});

	yield takeEvery('GET_CLAIMS', function* ({ payload: { contractId } }) {
		const account = yield select((state) => state.wallet.account);
		if (!account) {
			MODE === 'development' && console.warn('No account to GET_CLAIMS');
		} else {
			yield put({ type: ERROR.REMOVE, payload: 'GET_CLAIMS' });
			yield put({ type: LOADING.ADD, payload: 'GET_CLAIMS' });
			try {
				const result = yield call(web3Helper.getClaims, contractId, account);
				yield put({ type: 'SET_CLAIMS', payload: result });
			} catch (e) {
				MODE === 'development' && console.error(e);
				yield put({ type: ERROR.ADD, payload: { name: 'GET_CLAIMS', value: e } });
			}
			yield put({ type: LOADING.REMOVE, payload: 'GET_CLAIMS' });
		}
	});

	yield takeLatest('GET_ACCOUNT', function* ({ payload }) {
		yield put({ type: ERROR.REMOVE, payload: 'GET_ACCOUNT' });
		yield put({ type: LOADING.ADD, payload: 'GET_ACCOUNT' });
		if(payload && payload.provider) {
			try {
				web3Helper.initProvider(payload.provider);
			} catch (e) {
				console.error('Error on setup provider!', e);
			}
		}
		try {
			const {
				res: { accounts, network },
				timeout,
			} = yield race({
				res: call(web3Helper.getAccounts),
				timeout: delay(40000),
			});
			if (accounts) {
				setCookie('GegWalletConnected', true, { expires: 31556952 });
				yield put({ type: 'SET_ACCOUNT', payload: accounts[0] });
			}
			if (network) {
				yield put({ type: 'SET_NETWORK', payload: network });
			}
		} catch (e) {
			setCookie('GegWalletConnected', false);
			yield put({ type: ERROR.ADD, payload: { name: 'GET_ACCOUNT', value: e } });
		}
		yield put({ type: LOADING.REMOVE, payload: 'GET_ACCOUNT' });
	});

	yield takeLatest('GET_BALANCE', function* ({ payload }) {
		yield put({ type: ERROR.REMOVE, payload: 'GET_BALANCE' });
		yield put({ type: LOADING.ADD, payload: 'GET_BALANCE' });
		try {
			const balance = yield call(web3Helper.getBalance, payload);
			yield put({ type: 'SET_BALANCE', payload: balance });
		} catch (e) {
			MODE === 'development' && console.error(e);
			yield put({ type: ERROR.ADD, payload: { name: 'GET_BALANCE', value: e } });
		}
		yield put({ type: LOADING.REMOVE, payload: 'GET_BALANCE' });
	});

	yield takeLatest('SET_WALLET_DEPOSITS', fetchProductsDeposits);

	yield takeLatest('SET_ACCOUNT', function* () {
		const { account, tokens } = yield select((state) => ({
			account: state.wallet.account,
			tokens: state.tokens.entries.filter((t) => t.erc20token && Boolean(t.contract_id)),
		}));
		// console.log('account', account);
		// console.log('tokens', tokens);
		if (account) {
			yield all(
				tokens.map(function* (token) {
					try {
						const value = yield call(Erc20Web3Helper.getTokenBalance, {
							account,
							token: token.contract_id,
							symbol: token.symbol,
						});
						yield put({ type: 'SET_TOKEN_BALANCE', payload: { token: token.symbol, value } });
					} catch (e) {
						console.log(`Error on get balance for ${token.symbol}`, token);
						MODE === 'development' && console.error(e);
						//debugger;
					}
				}),
			);
		}
	});

	yield takeLatest('SET_AUTORENEWAL', function* ({ payload: { contractId, depositId, from, autoRenewal } }) {
		yield put({ type: 'SET_RENEWAL_LOADER', payload: { depositId, contractId } });
		const res = yield call(web3Helper.setAutoRenewal, { contractId, depositId, from, autoRenewal });
		// yield put({ type: 'GET_AUTORENEWAL', payload: {contractId, depositId, from } });
		yield put({ type: 'UNSET_RENEWAL_LOADER', payload: { depositId, contractId } });
		yield put({ type: 'SET_DEPOSIT_AUTORENEWAL', payload: { depositId, contractId, value: autoRenewal } });
	});

	yield takeLatest('GET_AUTORENEWAL', function* ({ payload: { contractId, depositId, from } }) {
		const value = yield call(web3Helper.getAutoRenewal, { contractId, depositId, from });
		console.log('GET_AUTORENEWAL', value);
		yield put({ type: 'SET_DEPOSIT_AUTORENEWAL', payload: { contractId, depositId, value } });
	});
}

const fetchProductsDeposits = function* () {
	const { products, account } = yield select((state) => ({
		account: state.wallet.account,
		products: ProductsForGetDeposit(state),
	}));
	// console.log('PRODUCTS', products);
	if (account) {
		yield all(
			products.map(function* (p) {
				yield put({ type: 'GET_WITHDRAWAL', payload: p });
				yield put({
					type: 'GET_DEPOSIT',
					payload: p,
				});
				// yield put({type: 'GET_AUTORENEWAL', payload: { contractId: p.contractId, depositId: } })
			}),
		);
	}
};
