import { takeLatest, take, call, put } from 'redux-saga/effects';
import WalletConnect from '@walletconnect/client';
import QRCodeModal from '@walletconnect/qrcode-modal';
import { deleteCookie, getCookie, setCookie } from '../../tools/cookieHelper';
import { channel } from 'redux-saga';

let connector = null;
const cookieName = 'GegWalletConnectConnected';
const wlChannel = channel();

export const watchWlChannel = function* () {
	while (true) {
		const action = yield take(wlChannel);
		yield put(action);
	}
};

export default function* () {
	const cookie = getCookie(cookieName);
	yield takeLatest('WALLET_CONNECT_INIT', function* () {
		const bridge = 'https://bridge.walletconnect.org';

		// create new connector
		connector = new WalletConnect({ bridge, qrcodeModal: QRCodeModal });

		console.log(connector, connector.connected);
		// check if already connected
		if (!connector.connected) {
			// create new session
			yield call(connector.createSession);
		}
		// subscribe to events
		connector.on('session_update', async (error, payload) => {
			console.log(`connector.on("session_update")`);

			if (error) {
				throw error;
			}

			const { chainId, accounts } = payload.params[0];
			wlChannel.put({ type: 'WALLET_CONNECT_SESSION_UPDATE', payload: { accounts, chainId } });
		});

		connector.on('connect', async (error, payload) => {
			console.log(`connector.on("connect")`);

			if (error) {
				throw error;
			}

			wlChannel.put({ type: 'WALLET_CONNECT_ON_CONNECT', payload });
		});

		connector.on('disconnect', async (error, payload) => {
			console.log(`connector.on("disconnect")`);

			if (error) {
				throw error;
			}
			// await onDisconnect(payload);
		});

		if (connector.connected) {
			const { chainId, accounts } = connector;
			wlChannel.put({ type: 'WALLET_CONNECT_SESSION_UPDATE', payload: { accounts, chainId } });
		}
	});

	yield takeLatest('WALLET_CONNECT_ON_CONNECT', function* ({ payload }) {
		const { accounts } = payload.params[0];
		const address = accounts[0];
		yield put({ type: 'SET_ACCOUNT', payload: address });
		setCookie(cookieName, true, { expires: 31556952 });
	});

	yield takeLatest('WALLET_CONNECT_SESSION_UPDATE', function* ({ payload: { accounts, chainId } }) {
		console.log('onSessionUpdate');
		const address = accounts[0];
		yield put({ type: 'SET_ACCOUNT', payload: address });
		setCookie(cookieName, true, { expires: 31556952 });
	});
	yield takeLatest('WALLET_CONNECT_KILL', function* () {
		if (connector) {
			connector.killSession();
		}
		deleteCookie(cookieName);
	});

	if (cookie) {
		yield put({ type: 'WALLET_CONNECT_INIT' });
	}
}

/*

	const getAccountAssets = async () => {
		const { address, chainId } = state;
		setState({ fetching: true });
		try {
			const assets = await apiGetAccountAssets(address, chainId);
			const { balance } = assets[0];
			dispatch({ type: 'SET_BALANCE', payload: web3.utils.fromWei(balance) });
			await setState({ ...state, fetching: false, address, assets });
		} catch (e) {
			console.error(e);
			await setState({ ...state, fetching: false });
		}
	};
 */
