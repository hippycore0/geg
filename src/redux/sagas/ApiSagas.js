import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ERROR, LOADING } from '../actionTypes';
import apiRequest from '../../tools/apiRequest';
import { getCookie } from '../../tools/cookieHelper';
const walletConnected = getCookie('GegWalletConnected');

function CommonApiListSaga(url, fetchAction, setAction) {
	return takeLatest(fetchAction, function* () {
		yield put({ type: LOADING.ADD, payload: fetchAction });
		yield put({ type: ERROR.REMOVE, payload: fetchAction });
		try {
			const payload = yield call(apiRequest, { url });
			yield put({ type: LOADING.REMOVE, payload: fetchAction });
			yield put({ type: setAction, payload });
		} catch (e) {
			console.error(e);
			yield put({ type: ERROR.ADD, payload: { name: fetchAction, value: e.message } });
		}
	});
}

function* regWallet({ payload }) {
	try {
		yield call(apiRequest, { url: '/wallets/', method: 'POST', data: { address: payload } });
		const response = yield call(apiRequest, { url: `/wallet-products/${payload}` });
		const { wallet_products, ...totals } = response;
		yield put({ type: 'SET_WALLET_DEPOSITS', payload: wallet_products });
		yield put({ type: 'SET_WALLET_TOTALS', payload: totals });
		const allowedProducts = yield call(apiRequest, { url: `/products/?wallet=${payload}` });
		yield put({ type: 'SET_ALLOWED_PRODUCTS', payload: allowedProducts });
	} catch (e) {
		console.error('Error while reg wallet', e);
	}
}

function* GetApiResourcesOnInit() {
	yield put({ type: 'FETCH_PRODUCTS' });
	yield put({ type: 'FETCH_TOKENS' });
	if (walletConnected) {
		yield put({ type: 'GET_ACCOUNT' });
	}
	yield put({ type: 'FETCH_PRODUCTS_DEPOSIT' });
	yield put({ type: 'FETCH_PRODUCTS_TOTALS' });
	yield put({ type: 'FETCH_EXCHANGES' });
	yield put({ type: 'FETCH_FAQ' });
	yield put({ type: 'FETCH_ORACLE' });
	yield put({ type: 'FETCH_RATES' });
	// const res = yield call(apiRequest, { url: `/signature/grUSDT1D/`, method: 'GET' });
	// console.log('signature', res);
}


function* ApiSagas() {
	yield CommonApiListSaga('/tokens/', 'FETCH_TOKENS', 'SET_TOKENS');
	yield CommonApiListSaga('/products/', 'FETCH_PRODUCTS', 'SET_PRODUCTS');
	yield CommonApiListSaga('/product-deposit/', 'FETCH_PRODUCTS_DEPOSIT', 'SET_PRODUCTS_DEPOSIT');
	yield CommonApiListSaga('/product-totals', 'FETCH_PRODUCTS_TOTALS', 'SET_PRODUCTS_TOTALS');
	yield CommonApiListSaga('/exchanges/', 'FETCH_EXCHANGES', 'SET_EXCHANGES');
	yield CommonApiListSaga('/faq-topic/', 'FETCH_FAQ', 'SET_FAQ');
	yield CommonApiListSaga('/oracle/', 'FETCH_ORACLE', 'SET_ORACLE');
	yield CommonApiListSaga('/exchange-rates/', 'FETCH_RATES', 'SET_RATES');
	yield GetApiResourcesOnInit();
	yield takeLatest('SET_ACCOUNT', regWallet);
	yield takeLatest('FETCH_SIGNATURE', function* ({ payload: { symbol }}) {
		const result = yield call(apiRequest, { url: `/signature/${symbol}` });
		yield put({ type: 'SET_TOKEN_SIGNATURE', payload: { symbol, data: result} });
	})
}

export default ApiSagas;
