import { all } from 'redux-saga/effects';
import { Web3Sagas, watchTransactionChannel } from './Web3Sagas';
import ApiSagas from './ApiSagas';
import WalletConnectSaga, { watchWlChannel } from './WalletConnectSaga';

const AppSaga = function* () {
	yield all([ApiSagas(), Web3Sagas(), WalletConnectSaga(), watchWlChannel(), watchTransactionChannel()]);
};

export default AppSaga;
