export const ACCOUNT = {
	GET: 'GET_ACCOUNT',
};

export const BALANCE = {
	GET: 'GET_BALANCE',
};

export const LOADING = {
	ADD: 'ADD_LOADING',
	REMOVE: 'REMOVE_LOADING',
};

export const ERROR = {
	ADD: 'ADD_ERROR',
	REMOVE: 'REMOVE_ERROR',
};
