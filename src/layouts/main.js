import React from 'react';
import styled from 'styled-components';
import AppHeader from '../components/AppHeader';
import AppFooter from '../components/AppFooter';
import CookieApprove from '../components/CookieApprove';
import BookmarkIt from '../components/BookmarkIt';
import WalletConnectorMobile from "../components/WalletConnector/WalletConnectorMobile";
const LayoutWrapper = styled.main`
	display: flex;
	min-height: 100vh;
	flex-direction: column;
	justify-content: space-between;
`;

const MainLayout = ({ children, maxWidth = 'lg' }) => {
	return (
		<LayoutWrapper>
			<div>
				<BookmarkIt />
				<AppHeader maxWidth={maxWidth} />
				{children}
				{/*<Container maxWidth={maxWidth}>{children}</Container>*/}
			</div>
			<AppFooter maxWidth={maxWidth} />
			<WalletConnectorMobile />
			<CookieApprove />
		</LayoutWrapper>
	);
};

export default MainLayout;
