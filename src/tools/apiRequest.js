import { getCookie } from './cookieHelper';
export default async ({ url, method = 'GET', data }) => {
	const headers = {
		'Content-Type': 'application/json',
		'X-CSRFToken': getCookie('csrftoken'),
	};

	const options = { method, headers };
	if (data) {
		options.body = JSON.stringify(data);
	}

	const response = await fetch(`${API_URL}${url}`, options);
	if (response.status >= 400) {
		throw new Error(response.statusText);
	}
	return await response.json();
};
