export default (num, symbols = 2) =>
	Math.round((Number(num) + Number.EPSILON) * Math.pow(10, symbols)) / Math.pow(10, symbols);
