import React from 'react';
import { createMuiTheme } from '@material-ui/core/styles';

// https://www.sipios.com/blog-tech/how-to-use-styled-components-with-material-ui-in-a-react-app#:~:text=Why%20Material%20UI%20and%20styled,that%20emulates%20Google's%20Material%20Design.&text=Styled%2Dcomponents%20is%20another%20great,styled%E2%80%9D%20components%20without%20CSS%20classes.
export default createMuiTheme({
	typography: {
		fontFamily: ['Montserrat', 'Roboto', 'Arial', 'sans-serif'].join(','),
		h5: {
			fontWeight: 600,
			color: '#484848',
		},
		h6: {
			color: '#484848',
			fontSize: 20,
			fontWeight: 500,
			lineHeight: 1.5,
		},
		color: '#484848',
	},
	palette: {
		primary: {
			main: '#71bd5a',
			contrastText: '#fff',
		},
		secondary: {
			// B6A421
			main: '#71bd5a',
		},
		error: {
			main: '#F44336',
		},
		background: {
			default: '#f7f7f7',
		},
	},
	overrides: {
		MuiButton: {
			root: {
				borderRadius: 10,
				lineHeight: 1.4,
				textTransform: 'none',
				fontWeight: 700,
			},
		},
		MuiPaper: {
			root: {
				color: '#484848',
			},
			elevation1: {
				boxShadow: '0px 2px 5px rgba(0, 0, 0, 0.07)',
			},
		},
	},
	props: {
		MuiButton: {
			disableElevation: true,
		},
		MuiButtonGroup: {
			disableElevation: true,
		},
		MuiPaper: {
			elevation: 1,
		},
	},
});
