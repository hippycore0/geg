import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import { FormattedMessage } from 'react-intl';

const Loader = () => {
	return (
		<div
			style={{
				padding: 32,
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				justifyContent: 'center',
			}}
		>
			<CircularProgress style={{ marginBottom: 8 }} />
			<div>
				<FormattedMessage id="Loader.message" defaultMessage="Loading..." />
			</div>
		</div>
	);
};

export default Loader;
