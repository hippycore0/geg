import React from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import Button from '@material-ui/core/Button';
import EtherscanLink from '../EtherscanLink';
import { Link as RouterLink } from 'react-router-dom';
import { FormattedMessage } from "react-intl";

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: 300,
		textAlign: 'center',
		paddingTop: theme.spacing(2),
		paddingBottom: theme.spacing(2),
	},
	smallSpacer: {
		marginBottom: theme.spacing(2),
	},
	spacer: {
		marginBottom: theme.spacing(4),
	},
}));

const renderTxMessage = ({ isLoading, transaction, isErc20 }) => {
	switch (true) {
		case !transaction && isErc20:
			return 'Please wait...';
		case Boolean(transaction) && Boolean(transaction.message):
			return transaction.message;
		case isErc20 && Boolean(transaction) && !transaction.message:
			return 'Transaction is processed. Waiting for Ethereum blockchain confirmation.';
		case !isErc20 && isLoading && !transaction:
			return 'Confirm transaction.';
		case !isErc20 && isLoading && Boolean(transaction):
			return 'Transaction is processed. Waiting for Ethereum blockchain confirmation.';
		case !isErc20 && !isLoading && Boolean(transaction):
			return 'Transaction completed.';
		default:
			console.log('render message', { isLoading, transaction, isErc20 });
			// debugger;
			return 'Unknown transaction state';
	}
};
const TransactionLoading = ({ isLoading, transaction, onClose, closeBtnText = 'OK',  depositWindow, isErc20 }) => {
	const classes = useStyles();
	const dispatch = useDispatch();

	if (!isLoading && !transaction) {
		return null;
	}
	 console.log('tx', transaction);
	return (
		<div className={classes.root}>
			<DialogTitle>
				{renderTxMessage({ isLoading, transaction, isErc20 })}
				{/*
				<Box display="flex" alignItems="center">
					<Box flexGrow={1}></Box>
					<Box>
						<IconButton
							onClick={(e) => {
								dispatch({ type: 'SET_TRANSACTION_CONFIRMED', payload: transaction.transactionHash });
								onClose(e);
							}}
						>
							<CloseIcon />
						</IconButton>
					</Box>
				</Box>*/}
			</DialogTitle>
			{transaction && transaction.transactionHash && (
				<div className={classes.spacer}>
					<EtherscanLink hash={transaction.transactionHash} type="tx" />
				</div>
			)}
			{isLoading && (
				<DialogContent className="spacer">
					<CircularProgress />
				</DialogContent>
			)}
			{!isLoading && transaction && (
				<Button
					color="primary"
					variant="contained"
					component={RouterLink}
					to="/finance"
					fullWidth
					onClick={(e) => {
						dispatch({ type: 'SET_TRANSACTION_CONFIRMED', payload: transaction.transactionHash });
						onClose(e);
					}}
				>
					{closeBtnText}
				</Button>
			)}
			{depositWindow && !isLoading && transaction && (
				<Button
					color="primary"
					variant="outlined"
					style={{ marginTop: 16 }}
					fullWidth
					onClick={(e) => {
						dispatch({ type: 'SET_TRANSACTION_CONFIRMED', payload: transaction.transactionHash });
						onClose(e);
					}}
				>
					<FormattedMessage id="TransactionLoading.closeBtn" defaultMessage="Close" />
				</Button>
			)}
		</div>
	);
};

export default TransactionLoading;
