import React from 'react';
import { makeStyles } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import EtherscanLink from '../EtherscanLink';
import TransactionLoading from './TransactionLoading';
import GradientBtn from '../GradientBtn';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import { DateTime } from 'luxon';
import HelpPopup from '../HelpPopup';
import { useIntl, FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: 300,
		paddingBottom: theme.spacing(2),
		'& .params-wrap': {
			fontSize: 13,
			background: '#f2f2f2',
			color: '#6e6e6e',
			lineHeight: 1.6,
			border: '1px solid #d7d7d7',
			boxSizing: 'border-box',
			borderRadius: '5px',
			padding: '6px 8px',
			marginBottom: '24px',
		},
	},
	spacer: {
		marginBottom: theme.spacing(4),
	},
	smallSpacer: {
		marginBottom: theme.spacing(2),
	},
	tokenRow: {
		display: 'flex',
		justifyContent: 'space-between',
		marginBottom: theme.spacing(1),
	},
	tokenRowEven: {
		borderBottom: '1px solid rgba(33, 33, 33, 0.08)',
		paddingBottom: theme.spacing(1),
	},
	tokenLogo: { height: 32, width: 32, marginRight: 8, verticalAlign: 'middle' },
	listParams: {
		padding: 0,
		margin: 0,
		listStyle: 'none',
	},
	infoIcon: {
		color: '#888',
		fontSize: 16,
		marginTop: -2,
		verticalAlign: 'middle',
		cursor: 'pointer',
	},
}));

const DepositTab = ({ product, onClose }) => {
	const classes = useStyles();
	const intl = useIntl();
	const [state, setState] = React.useState({
		error: null,
		depositValue: product.min_locked_amount,
		autoRenewal: false,
		showHelpPopup: false,
		helpPopupText: null,
	});
	const { account, balance, tokenBalance } = useSelector((state) => state.wallet);
	const activeTransaction = useSelector((state) =>
		state.transactions.find((t) => !t.confirmed && product.id === t.productId),
	);
	const makeDepositLoading = useSelector(({ loading }) => loading.indexOf('MAKE_DEPOSIT') >= 0);
	const dispatch = useDispatch();
	React.useEffect(() => {
		/*
		 * VALIDATE AMOUNT
		 * */
		if (parseFloat(state.depositValue) < parseFloat(product.min_locked_amount)) {
			setState({
				...state,
				error: intl.formatMessage(
					{ id: 'DepositTab.fieldAmountErrorMin', defaultMessage: `Minimum: {val} {symbol}` },
					{ val: product.min_locked_amount, symbol: product.token_product_symbol },
				),
			});
		} else if (product.tokensLeft && parseFloat(state.depositValue) > parseFloat(product.tokensLeft)) {
			setState({
				...state,
				error: intl.formatMessage(
					{ id: 'DepositTab.fieldAmountErrorLimit', defaultMessage: `Pool limit: {val} {symbol}` },
					{ val: product.tokensLeft, symbol: product.token_product_symbol },
				),
			});
		} else {
			setState({ ...state, error: null });
		}
	}, [state.depositValue]);

	// console.log('PRODUCT', product);

	const handleChangeDepositValue = (depositValue) => setState({ ...state, depositValue });

	const handleClickMaxValue = () => {
		if (!product.erc20token) {
			const balanceValue = balance - (balance / 100) * 2;
			handleChangeDepositValue(
				product.tokensLeft && product.tokensLeft < balanceValue ? product.tokensLeft : balanceValue,
			);
		} else {
			const tokenProductBalance = tokenBalance[product.token_product_symbol];
			console.log(`Balance of ${product.token()}`, tokenProductBalance);
			if (tokenProductBalance) {
				handleChangeDepositValue(
					product.tokensLeft && product.tokensLeft < tokenProductBalance
						? product.tokensLeft
						: Math.round(tokenProductBalance),
				);
			}
		}
	};

	if (makeDepositLoading || activeTransaction) {
		return (
			<TransactionLoading
				isLoading={makeDepositLoading}
				isErc20={product.erc20token}
				transaction={activeTransaction}
				onClose={onClose}
				closeBtnText={'View deposit'}
				depositWindow={true}
			/>
		);
	}

	return (
		<>
			<HelpPopup
				text={state.helpPopupText}
				open={state.showHelpPopup}
				onClose={() => setState({ ...state, showHelpPopup: false })}
			/>
			<div className={classes.root}>
				<p>
					<FormattedMessage
						id="DepositTab.description"
						defaultMessage={'Push "Deposit" to get high yield with fixed interest rate and daily accrual.'}
					/>{' '}
				</p>
				<div className="params-wrap">
					<ul className={classes.listParams}>
						<li>
							<FormattedMessage id="DepositTab.depositPeriodLabel" defaultMessage={'Deposit period'} />:{' '}
							{product.days === 1 ? `Unlimited` : `${product.days} (${product.period})`}
						</li>
						<li>
							<FormattedMessage id="DepositTab.depositEndDate" defaultMessage={'Deposit end date'} />:{' '}
							{DateTime.local().plus({ days: product.days }).toLocaleString(DateTime.DATE_MED)}
						</li>
						<li>
							<FormattedMessage id="DepositTab.depositCurrency" defaultMessage={'Deposit currency'} />:{' '}
							{product.token_product_symbol}
						</li>
						<li>
							<FormattedMessage id="DepositTab.fixedAPY" defaultMessage={'Fixed APY'} />{' '}
							<InfoIcon
								onClick={() =>
									setState({
										...state,
										showHelpPopup: true,
										helpPopupText: (
											<FormattedMessage
												id="DepositTab.fixedAPYHelpText"
												defaultMessage={
													'Annual Percentage Yield calculated on the base of daily accrual in deposit currency'
												}
											/>
										),
									})
								}
								className={classes.infoIcon}
							/>
							: {product.apy}%
						</li>
						<li>
							<FormattedMessage
								id="DepositTab.interestPeriod"
								defaultMessage={'Interest redemption period'}
							/>
							: {product.payout_interval_formatted}
						</li>
						<li>
							<FormattedMessage id="DepositTab.rewardCurrency" defaultMessage={'Reward currency'} />{' '}
							<InfoIcon
								onClick={() =>
									setState({
										...state,
										showHelpPopup: true,
										helpPopupText: (
											<FormattedMessage
												id="DepositTab.rewardCurrencyHelpText"
												defaultMessage={
													'Reward currency is GEG calculated on the base of exchange rate on interest claim / reinvestment date'
												}
											/>
										),
									})
								}
								className={classes.infoIcon}
							/>
							: {product.token_reward_symbol}
						</li>
					</ul>
				</div>
				{product.days > 1 && (<div className="spacer">
					<FormattedMessage id="DepositTab.depositAutoRenewalLabel" defaultMessage="Deposit auto-renewal">
						{(msg) => (
							<FormControlLabel
								label={msg}
								control={
									<Switch
										checked={state.autoRenewal}
										onChange={(e) => setState({ ...state, autoRenewal: e.target.checked })}
										name="deposit-checkbox"
										color="primary"
									/>
								}
							/>
						)}
					</FormattedMessage>
				</div>)}
				<div className="spacer">
					{product.contract_id && (
						<EtherscanLink hash={product.contract_id}>
							<FormattedMessage
								id="DepositTab.etherscanLink"
								defaultMessage={'Read Etherscan contract'}
							/>
						</EtherscanLink>
					)}
				</div>

				<div className="spacer">
					<TextField
						fullWidth
						error={Boolean(state.error)}
						type="number"
						value={state.depositValue}
						onChange={(e) => handleChangeDepositValue(e.target.value)}
						inputProps={{ min: 0 }}
						InputProps={{
							startAdornment: (
								<InputAdornment position="start">{product.token_product_symbol}</InputAdornment>
							),
							endAdornment: (
								<InputAdornment position="end">
									<Button color="primary" onClick={handleClickMaxValue}>
										MAX
									</Button>
								</InputAdornment>
							),
						}}
						helperText={
							state.error || (
								<FormattedMessage
									id="DepositTab.fieldAmountHelpText"
									defaultMessage={'Enter the amount or press MAX'}
								/>
							)
						}
					/>
				</div>
				<GradientBtn
					color="primary"
					variant="contained"
					fullWidth
					disabled={!product.contract_id || Boolean(state.error) || !Boolean(state.depositValue)}
					onClick={() =>
						dispatch({
							type: 'MAKE_DEPOSIT',
							payload: {
								contractId: product.contract_id,
								from: account,
								productId: product.id,
								token: product.erc20token ? product.token_product_contract : null,
								value: state.depositValue,
								autoRenewal: state.autoRenewal,
							},
						})
					}
				>
					<FormattedMessage
						id="DepositTab.depositBtn"
						defaultMessage={'Make deposit'}
					/>
				</GradientBtn>
			</div>
		</>
	);
};

export default DepositTab;
