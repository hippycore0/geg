import React from 'react';
import WithdrawalTab from './WithdrawalTab';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
	root: {
		'& .MuiDialog-paper': {
			width: 320,
		},
		'& .spacer': {
			marginBottom: theme.spacing(2),
		},
	},
}));

const WithdrawalDialog = ({ onClose, open, product, deposit }) => {
	const classes = useStyles();
	return (
		<Dialog onClose={onClose} className={classes.root} open={open} maxWidth="xs">
			<Box display="flex" alignItems="center">
				<Box flexGrow={1}></Box>
				<Box>
					<IconButton onClick={onClose}>
						<CloseIcon />
					</IconButton>
				</Box>
			</Box>
			<DialogContent>
				<WithdrawalTab product={product} onClose={onClose} deposit={deposit} showSelectDeposit={false} />
			</DialogContent>
		</Dialog>
	);
};

export default WithdrawalDialog;
