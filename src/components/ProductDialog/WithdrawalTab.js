import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Alert, AlertTitle } from '@material-ui/lab';
import EtherscanLink from '../EtherscanLink';
import TransactionLoading from './TransactionLoading';
import GradientBtn from '../GradientBtn';
import { DateTime } from 'luxon';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: 300,
		paddingBottom: theme.spacing(2),
	},
	penalty: {
		marginBottom: theme.spacing(2),
	},
	smallSpacer: {
		marginBottom: theme.spacing(2),
	},
	spacer: {
		marginBottom: theme.spacing(4),
	},
}));

const WithdrawalTab = ({ product, onClose, deposit, showSelectDeposit = true }) => {
	const classes = useStyles();
	const account = useSelector((state) => state.wallet.account);
	const dispatch = useDispatch();
	const makeWithdrawLoading = useSelector(({ loading }) => loading.indexOf('MAKE_WITHDRAWAL') >= 0);
	const activeTransaction = useSelector((state) =>
		state.transactions.find((t) => !t.confirmed && product.id === t.productId),
	);
	const defaultDepositIndex = !deposit ? 0 : product.deposits.findIndex((d) => d.deposit_id === deposit.deposit_id);
	const [activeDepositIndex, setActiveDepositIndex] = React.useState(defaultDepositIndex);
	const activeDeposit = product.deposits[activeDepositIndex];
	const activeDepositPayoutDate =
		DateTime.fromSeconds(activeDeposit.timestamp).plus({ days: product.payout_interval }).diffNow('days').days < 0;
	const withdrawalPayload = {
		contractId: product.contract_id,
		from: account,
		token: product.erc20token ? product.token_reward_symbol : null,
		depositId: activeDeposit.deposit_id,
		productId: product.id,
		value: activeDeposit.value,
	};
	const makePenaltyWithdrawal = () =>
		dispatch({
			type: 'MAKE_WITHDRAWAL',
			payload: withdrawalPayload,
		});

	if (makeWithdrawLoading || activeTransaction) {
		return <TransactionLoading isLoading={makeWithdrawLoading} transaction={activeTransaction} onClose={onClose} />;
	}
	// console.log('activeDepositIndex', product.deposits[activeDepositIndex]);
	// console.log('activeDepositPayoutDate', activeDepositPayoutDate);
	return (
		<div className={classes.root}>
			{!activeDepositPayoutDate && (
				<Alert severity="warning" className={classes.penalty}>
					<AlertTitle>Warning</AlertTitle>
					{product.penalty || 'You have a penalty'}
				</Alert>
			)}
			{activeDepositPayoutDate && (
				<Alert severity="success" className={classes.penalty}>
					<AlertTitle><FormattedMessage id="WithdrawalTab.activeDepositPayoutDateHeader" defaultMessage="Information" /></AlertTitle>
					<FormattedMessage id="WithdrawalTab.activeDepositPayoutDate" defaultMessage="When you redeem your deposit, you receive an accumulated yield in one transaction." />
				</Alert>
			)}

			{showSelectDeposit && (
				<>
					<Typography variant="subtitle2" className={classes.smallSpacer}>
						<FormattedMessage id="WithdrawalTab.selectDeposit" defaultMessage="Select deposit" />
					</Typography>
					<div className={classes.spacer}>
						<Table size="small" aria-label="a dense table">
							<TableHead>
								<TableRow>
									<TableCell>
										<FormattedMessage id="WithdrawalTab.depositTableValue" defaultMessage="Value" />
									</TableCell>
									<TableCell>
										<FormattedMessage id="WithdrawalTab.depositTableHash" defaultMessage="Hash" />
									</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{product.deposits.map((d, i) => (
									<TableRow
										key={`${product.id}_${d.deposit_id}`}
										onClick={() => setActiveDepositIndex(i)}
										style={{
											cursor: 'pointer',
											backgroundColor: i === activeDepositIndex ? '#bde6cc' : 'transparent',
										}}
									>
										<TableCell style={{ whiteSpace: 'nowrap' }}>
											{d.value} {product.token_product_symbol}
										</TableCell>
										<TableCell>
											<EtherscanLink hash={d.transactionHash} type="tx" />
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</div>
				</>
			)}

			<GradientBtn
				color="primary"
				variant="contained"
				disabled={!product.contract_id}
				fullWidth
				onClick={makePenaltyWithdrawal}
			>
				{activeDepositPayoutDate ? (
					<FormattedMessage id="WithdrawalTab.activeDepositBtn" defaultMessage="Bring out all" />
				) : (
					<FormattedMessage id="WithdrawalTab.unActiveDepositBtn" defaultMessage="Get withdrawal" />
				)}
			</GradientBtn>
		</div>
	);
};

export default WithdrawalTab;
