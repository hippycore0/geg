import React from 'react';
import { useSelector } from 'react-redux';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { makeStyles } from '@material-ui/core';
import DepositTab from './DepositTab';
import WithdrawalTab from './WithdrawalTab';
import WalletConnectorDialog from '../WalletConnector/WallectConnectorDialog';
import { useIntl, FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles((theme) => ({
	root: {
		'& .MuiDialog-paper': {
			width: 320,
		},
		'& .spacer': {
			marginBottom: theme.spacing(2),
		},
	},
}));

const TabPanel = (props) => {
	const { children, value, index, ...other } = props;
	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}
		>
			{value === index && <DialogContent>{children}</DialogContent>}
		</div>
	);
};

const ProductDialog = ({ onClose, open, product, openTab = 'deposit' }) => {
	const [activeTab, setActiveTab] = React.useState(openTab === 'deposit' ? 0 : 1);
	const classes = useStyles();
	const intl = useIntl();
	const dispatch = useDispatch();
	const { account } = useSelector(({ wallet }) => wallet);
	const activeTransaction = useSelector((state) =>
		state.transactions.find((t) => !t.confirmed && product.id === t.productId),
	);
	React.useEffect(() => {
		setActiveTab(openTab === 'deposit' ? 0 : 1);
	}, [open]);

	if (!product) {
		return null;
	}
	if (!account) {
		return <WalletConnectorDialog isOpen={open} onClose={onClose} />;
	}
	const hasDeposits = product.deposits && product.deposits.length > 0;
	return (
		<Dialog onClose={e=> {
			if(activeTransaction) {
				dispatch({ type: 'SET_TRANSACTION_CONFIRMED', payload: activeTransaction.transactionHash });
			}
			onClose(e);

		}} className={classes.root} open={open} maxWidth="xs">
			<Tabs variant="fullWidth" value={activeTab} onChange={(e, v) => setActiveTab(v)}>
				<Tab label={intl.formatMessage({id: 'ProductDialog.DepositTab', defaultMessage: 'Deposit'})} />
				<Tab disabled={!hasDeposits} label={intl.formatMessage({id: 'ProductDialog.WithdrawTab', defaultMessage: 'Withdraw'})} />
			</Tabs>
			<TabPanel value={activeTab} index={0}>
				<DepositTab product={product} onClose={onClose} />
			</TabPanel>
			<TabPanel value={activeTab} index={1}>
				<WithdrawalTab product={product} onClose={onClose} />
			</TabPanel>
		</Dialog>
	);
};

export default ProductDialog;
