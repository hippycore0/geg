import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import AppIntl from '../intl/provider';
import theme from '../theme';
import MainLayout from '../layouts/main';
import Routes from '../routes';
import configureStore, { history } from '../redux/store';
import AuthProvider from './AuthProvider';
const store = configureStore({});
const App = () => {
	return (
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<>
					<AppIntl>
						<ThemeProvider theme={theme}>
							<CssBaseline />
							<AuthProvider>
								<MainLayout>
									<Routes />
								</MainLayout>
							</AuthProvider>
						</ThemeProvider>
					</AppIntl>
				</>
			</ConnectedRouter>
		</Provider>
	);
};

export default App;
