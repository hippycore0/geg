import Dialog from '@material-ui/core/Dialog/Dialog';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import DialogActions from '@material-ui/core/DialogActions/DialogActions';
import Button from '@material-ui/core/Button';
import React from 'react';
import { FormattedMessage } from 'react-intl';

const InfoDialog = ({ open, text, onClose }) => (
	<Dialog open={open} onClose={onClose} maxWidth="xs">
		<DialogContent>{text}</DialogContent>
		<DialogActions>
			<Button onClick={onClose} color="primary">
				<FormattedMessage id="InfoDialog.approveBtn" defaultMessage="I understand" />
			</Button>
		</DialogActions>
	</Dialog>
);

export default InfoDialog;
