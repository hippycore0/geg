import React from 'react';
import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import BlockHeader from '../../components/BlockHeader';
import ProductDialog from '../ProductDialog';
import ProductEntry from './ProductEntry';
import ProductFilter from './ProductFilter';
import { groupByKey } from '../../tools/arrays';
import { ProductListSelector } from '../../redux/selectors';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		paddingBottom: theme.spacing(3),
		backgroundColor: '#e3e3e3',
		paddingTop: theme.spacing(3),
	},
}));

const ProductsList = () => {
	const classes = useStyles();
	const [state, setState] = React.useState({
		activeProduct: null,
		isOpen: false,
		dialogActiveTab: 'deposit',
	});
	const { products, productsFilter, productsIsLoading, tokensIsLoading } = useSelector((state) => ({
		products: ProductListSelector(state),
		productsFilter: state.products.filter,
		network: state.wallet.network,
		productsIsLoading: state.loading.indexOf('FETCH_PRODUCTS') > 0,
		tokensIsLoading: state.loading.indexOf('FETCH_TOKENS') > 0,
	}));

	const hasProducts = products && Object.keys(products).length > 0;
	// console.log(products);
	if (!hasProducts && !productsIsLoading) {
		return null;
	}

	if (productsIsLoading || tokensIsLoading) {
		return (
			<div style={{ textAlign: 'center' }}>
				<CircularProgress />
			</div>
		);
	}
	const filteredProducts = groupByKey(
		products.filter((product) => {
			if (productsFilter === 'All') {
				return true;
			}
			return product.token_product_symbol === productsFilter;
		}),
		'token_product_symbol',
	);

	const availableProducts = Object.keys(filteredProducts);

	const handleProductClick = (activeProduct, dialogActiveTab) =>
		setState({
			...state,
			isOpen: true,
			activeProduct,
			dialogActiveTab,
		});

	return (
		<div className={classes.root}>
			<ProductDialog
				// todo: set_transaction_confirmed if has active transaction for active product
				onClose={() => setState({ ...state, isOpen: false })}
				open={state.isOpen}
				product={state.activeProduct}
				openTab={state.dialogActiveTab}
			/>
			<Container>
				<BlockHeader controls={<ProductFilter products={products} />}>
					<FormattedMessage id="ProductsList.availableList" defaultMessage="Available offers" />
				</BlockHeader>
				<Grid container spacing={2}>
					{availableProducts.map((key) => (
						<Grid item xs={12} sm={6} md={6} lg={3} key={key}>
							<ProductEntry
								group={key}
								groupProducts={filteredProducts[key]}
								onClick={handleProductClick}
							/>
						</Grid>
					))}
				</Grid>
			</Container>
		</div>
	);
};

export default ProductsList;
