import React from 'react';
import { useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress/LinearProgress';
import numeral from 'numeral';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper/Paper';
import theme from '../../theme';
import CallMadeIcon from '@material-ui/icons/CallMade';
import HotIcon from '../../../assets/svg/hot.svg';
import GradientBtn from '../GradientBtn';
import { FormattedMessage } from 'react-intl';

const StyledInvestmentBlock = styled(Paper)`
	padding: 16px;
	border-radius: 29px;
	position: relative;
	box-shadow: -2px 2px 5px rgba(152, 152, 152, 0.35);
	background-color: #fcfcfc;
	.block-header {
		margin-bottom: ${theme.spacing(3)}px;
		display: flex;
		justify-content: space-between;
		align-items: flex-end;
	}
	.progress-wrap {
		margin-bottom: ${theme.spacing(3)}px;
		overflow: hidden;
	}
	.btn-wrap {
	}
	.btn-small-wrap {
		margin-bottom: ${theme.spacing(2)}px;
		& .MuiButton-root {
			padding: 2px 6px;
			border-radius: 4px;
			min-width: 100%;
			width: 100%;
			white-space: nowrap;
		}
		@media (min-width: 1280px) {
			height: 55px;
		}
	}
	.token-logo {
		height: 80px;
	}
	.block-title {
	}
	.contract-link {
		color: #cdcdcd;
		margin-left: 4px;
		& .MuiSvgIcon-root {
			font-size: 16px;
		}
	}
	.params-wrap {
		font-size: 14px;
		background: #f2f2f2;
		color: #6e6e6e;
		border: 0.5px solid #d7d7d7;
		box-sizing: border-box;
		border-radius: 15px;
		margin-bottom: 24px;
		padding: 4px 8px;
		.MuiGrid-item + .MuiGrid-item {
			font-weight: 600;
		}
		.MuiGrid-container {
			margin: 0.72em 0;
		}
	}
	.hot-icon {
		position: absolute;
		right: 20px;
		top: -15px;
	}
	.apy-value {
		color: ${theme.palette.primary.main};
	}
`;

const StyledLinearProgress = styled(LinearProgress)`
	background-color: #ececec;
	height: 5px;
	border-radius: 10px;
	& .MuiLinearProgress-bar {
		background-color: ${(props) =>
			props.value < 50
				? theme.palette.primary.main
				: props.value >= 50 && props.value < 90
				? '#F8A400'
				: '#C75B4D'};
	}
`;
function usePrevious(value) {
	const ref = React.useRef();
	React.useEffect(() => {
		ref.current = value;
	});
	return ref.current;
}
const ProductEntry = ({ group, groupProducts, onClick }) => {
	const dispatch = useDispatch();
	const [activeGroupProduct, setActiveGroupProduct] = React.useState(groupProducts[groupProducts.length - 1]);
	const prevGroups = usePrevious({groupProducts});
	React.useEffect(() => {
		// console.log(prevGroups);
		if(!prevGroups || prevGroups.groupProducts.length !== groupProducts.length ) {
			setActiveGroupProduct(groupProducts[groupProducts.length - 1]);
		}
	}, [groupProducts]);

	// console.log('activeGroupProduct', activeGroupProduct);
	const hasDeposits = activeGroupProduct.deposits && activeGroupProduct.deposits.length > 0;
	const handleDialogButtonClick = (activeTab) => {
		dispatch({
			type: 'GET_DEPOSIT',
			payload: {
				contractId: activeGroupProduct.contract_id,
				productId: activeGroupProduct.id,
				erc20: activeGroupProduct.erc20token,
			},
		});
		onClick(activeGroupProduct, activeTab);
	};
	return (
		<StyledInvestmentBlock>
			{activeGroupProduct.hot && <img src={HotIcon} alt="" className="hot-icon" />}
			<Box className="block-header">
				<img src={activeGroupProduct.logo} alt={group} className="token-logo" />
				<Typography variant="h6" className="block-title">
					{group}
					{activeGroupProduct.contractUrl && (
						<FormattedMessage id="ProductEntry.contractLinkTitle" defaultMessage="Read Etherscan contract">
							{(msg) => (
								<a
									href={activeGroupProduct.contractUrl}
									target="_blank"
									className="contract-link"
									title={msg}
								>
									<CallMadeIcon fontSize="small" />
								</a>
							)}
						</FormattedMessage>
					)}
				</Typography>
			</Box>
			<Box className="progress-wrap">
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						alignItems: 'flex-end',
						marginBottom: theme.spacing(1),
					}}
				>
					<div style={{ color: '#484848', lineHeight: 1, fontSize: 16, fontWeight: 500 }}>
						<FormattedMessage
							id="ProductEntry.depositVolumeFilled"
							defaultMessage="Deposit volume filled"
						/>
						:
					</div>
					<div style={{ fontSize: 11, fontWeight: 600 }}>
						{numeral(activeGroupProduct.progress).format('0.00')}%
					</div>
				</div>
				<StyledLinearProgress
					variant="determinate"
					value={activeGroupProduct.progress >= 100 ? 100 : activeGroupProduct.progress}
					color={activeGroupProduct.progress > 50 ? 'secondary' : 'primary'}
				/>
			</Box>
			<div style={{ color: '#484848', fontSize: 16, fontWeight: 500, marginBottom: 8 }}>
				<FormattedMessage id="ProductEntry.depositPeriod" defaultMessage="Deposit period" />:
			</div>
			<Box className="btn-small-wrap">
				<Grid container spacing={1}>
				{groupProducts.map((groupProduct) => (
					<Grid item xs={3} key={groupProduct.id}>
					<GradientBtn
						color="primary"
						key={groupProduct.id}
						onClick={() => setActiveGroupProduct(groupProducts.find((p) => p.id === groupProduct.id))}
						variant={groupProduct.id === activeGroupProduct.id ? 'contained' : 'outlined'}
					>
						{groupProduct.hot && '🔥'} {groupProduct.period}
					</GradientBtn>
					</Grid>
				))}
				</Grid>
			</Box>

			<Box className="params-wrap">
				<Grid container spacing={0} justify="space-between">
					<Grid item xs={6}>
						<FormattedMessage id="ProductEntry.totalSupply" defaultMessage="Total supply" />
					</Grid>
					<Grid item>
						{activeGroupProduct.pool_volume_token.toLocaleString()}{' '}
						{activeGroupProduct.token_product_symbol}
					</Grid>
				</Grid>
				<Grid container spacing={0} justify="space-between">
					<Grid item xs={6}>
						<FormattedMessage id="ProductEntry.depositPeriod" defaultMessage="Deposit period" />
					</Grid>
					<Grid item>{activeGroupProduct.days === 1 ? 'Unlimited' : `${activeGroupProduct.days} days`}</Grid>
				</Grid>
				<Grid container spacing={0} justify="space-between">
					<Grid item xs={6}>
						<FormattedMessage id="ProductEntry.depositAPY" defaultMessage="Deposit APY" />
					</Grid>
					<Grid item className="apy-value">
						{activeGroupProduct.apy}%
					</Grid>
				</Grid>
				<Grid container spacing={0} justify="space-between">
					<Grid item xs={6}>
						<FormattedMessage id="ProductEntry.rewardCurrency" defaultMessage="Reward currency" />
					</Grid>
					<Grid item>{activeGroupProduct.token_reward_symbol}</Grid>
				</Grid>
			</Box>
			<Box className="btn-wrap">
				<Grid container spacing={1} justify="space-between">
					<Grid item xs={6}>
						<GradientBtn
							onClick={() => handleDialogButtonClick('deposit')}
							variant="contained"
							color="primary"
							fullWidth
							disabled={activeGroupProduct.progress >= 100}
						>
							<FormattedMessage id="ProductEntry.depositBtn" defaultMessage="Deposit" />
						</GradientBtn>
					</Grid>
					<Grid item xs={6}>
						<GradientBtn
							disabled={!hasDeposits}
							onClick={() => handleDialogButtonClick('redeem')}
							variant={!hasDeposits ? 'outlined' : 'contained'}
							color="primary"
							fullWidth
						>
							<FormattedMessage id="ProductEntry.withdrawBtn" defaultMessage="Redeem" />
						</GradientBtn>
					</Grid>
				</Grid>
			</Box>
		</StyledInvestmentBlock>
	);
};
export default ProductEntry;
