import React from 'react';
import { makeStyles } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { useSelector, useDispatch } from 'react-redux';
import GradientBtn from '../GradientBtn';
import { groupByKey } from '../../tools/arrays';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		justifyContent: 'space-between',
		'& .MuiButton-root': {
			boxShadow: '1px 0px 4px rgba(0, 0, 0, 0.1)',
			position: 'relative',
			borderRadius: '10px',
			padding: '4px 18px',
			textTransform: 'uppercase',
			minWidth: '0',
			marginLeft: '-12px',
			'&:first-child': {
				marginLeft: 0,
			},
		},
		'& .MuiButton-contained': {
			// padding: '4px 12px',
			boxShadow: '1px 1px 5px rgba(31, 138, 52, 0.6)',
		},
	},
}));

const ProductFilter = ({ products }) => {
	const classes = useStyles();
	const groups = Object.keys(groupByKey(products, 'token_product_symbol')).concat('All').reverse();
	const activeFilter = useSelector((state) => state.products.filter);
	const dispatch = useDispatch();
	return (
		<Box className={classes.root}>
			{groups.map((alias, i) => (
				<GradientBtn
					key={alias}
					style={{ zIndex: activeFilter === alias ? groups.length : groups.length - i }}
					variant={activeFilter === alias ? 'contained' : 'outlined'}
					onClick={() => dispatch({ type: 'SET_PRODUCTS_FILTER', payload: alias })}
				>
					{alias}
				</GradientBtn>
			))}
		</Box>
	);
};

export default ProductFilter;
