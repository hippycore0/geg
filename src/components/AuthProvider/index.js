import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setCookie, getCookie } from '../../tools/cookieHelper';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogContent from '@material-ui/core/DialogContent';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';

const authCookie = getCookie('geg-login');

const useStyles = makeStyles((theme) => ({
	root: {},
	spacer: {
		marginBottom: theme.spacing(2),
	},
}));

const AuthProvider = ({ children }) => {
	const dispatch = useDispatch();
	const classes = useStyles();
	const isAuth = useSelector((state) => state.auth.isAuth);
	const [values, setValues] = React.useState({ showPassword: false, isValid: false, isTouched: false, pwd: '' });
	React.useEffect(() => {
		dispatch({ type: 'SET_IS_AUTH', payload: authCookie });
	}, []);

	const handleClickShowPassword = () => {
		setValues({ ...values, showPassword: !values.showPassword });
	};

	const handleChange = (prop) => (event) => {
		setValues({ ...values, [prop]: event.target.value });
	};

	const handleMouseDownPassword = (event) => {
		event.preventDefault();
	};

	const handleAuth = () => {
		const isValid = values.pwd === '8844557711';
		if (!isValid) {
			setValues({ ...values, isTouched: true });
		} else {
			setValues({ ...values, isValid });
			setCookie('geg-login', true, { expires: 31556952 });
			dispatch({ type: 'SET_IS_AUTH', payload: true });
		}
	};

	if (isAuth) {
		return children;
	}

	return (
		<Dialog open={true}>
			<DialogTitle className={classes.root}>Sign in</DialogTitle>
			<DialogContent style={{ paddingBottom: 16 }}>
				<form>
					<FormControl fullWidth className={classes.spacer}>
						<InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
						<Input
							id="pwd"
							type={values.showPassword ? 'text' : 'password'}
							value={values.pwd}
							onChange={handleChange('pwd')}
							error={values.isTouched && !values.isValid}
							helpertext={values.isTouched && !values.isValid ? 'Invalid password' : ''}
							endAdornment={
								<InputAdornment position="end">
									<IconButton
										aria-label="toggle password visibility"
										onClick={handleClickShowPassword}
										onMouseDown={handleMouseDownPassword}
									>
										{values.showPassword ? <Visibility /> : <VisibilityOff />}
									</IconButton>
								</InputAdornment>
							}
						/>
					</FormControl>
					<Button variant="contained" color="primary" fullWidth onClick={handleAuth}>
						Sign In
					</Button>
				</form>
			</DialogContent>
		</Dialog>
	);
};

export default AuthProvider;
