import React from 'react';
import CookieConsent from 'react-cookie-consent';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		zIndex: '1110 !important',
		backgroundColor: `#5DA854 !important`,
		alignItems: 'center !important',
		position: 'relative !important',
		'& a': {
			color: 'inherit',
		},
		[theme.breakpoints.down('sm')]: {
			flexDirection: 'row',
		},
	},
	content: {
		marginTop: '2px !important',
		marginBottom: '2px !important',
		[theme.breakpoints.down('sm')]: {
			flex: '1 !important',
		},
	},
	button: {
		backgroundColor: `transparent !important`,
		color: 'white !important',
		borderRadius: '10px !important',
		fontSize: '16px !important',
		marginTop: '2px !important',
		marginBottom: '2px !important',
		position: 'relative',
		top: '2px',
	},
}));
const BookmarkIt = () => {
	const classes = useStyles();
	return (
		<CookieConsent
			expires={150}
			location="top"
			cookieName="GegBookmarkIt"
			buttonText={<CloseIcon fontSize="inherit" />}
			contentClasses={classes.content}
			containerClasses={classes.root}
			buttonClasses={classes.button}
		>
			<FormattedMessage
				id="BookmarkIt.message"
				defaultMessage="Always make sure the URL is {domain} - bookmark it to be safe"
				values={{ domain: <b>app.geg.finance</b> }}
			/>
		</CookieConsent>
	);
};

export default BookmarkIt;
