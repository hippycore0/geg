import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import Link from '@material-ui/core/Link';
import CallMadeIcon from '@material-ui/icons/CallMade';

const useStyles = makeStyles((theme) => ({
	root: {
		whiteSpace: 'nowrap',
		'& .MuiSvgIcon-root': {
			fontSize: 12,
			marginLeft: 2,
			color: '#cdcdcd',
		},
	},
}));

const EtherscanLink = ({ hash, children, useIcon = true, type = 'address', full = false, ...props }) => {
	const classes = useStyles();
	if (!hash) {
		console.warn('no hash for Etherscan link');
		return null;
	}
	return (
		<Link
			className={classes.root}
			href={`https://ropsten.etherscan.io/${type}/${hash}`}
			target="_blank"
			title={`Read Etherscan ${type === 'address' ? 'contract' : 'transaction'}`}
			{...props}
		>
			{children || (full ? hash : `${hash.slice(0, 6)}...${hash.slice(-4)}`)}{' '}
			{useIcon && <CallMadeIcon fontSize="small" />}
		</Link>
	);
};

EtherscanLink.propTypes = {
	hash: PropTypes.string,
	type: PropTypes.oneOf(['address', 'tx']),
	full: PropTypes.bool,
};
export default EtherscanLink;
