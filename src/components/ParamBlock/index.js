import styled from 'styled-components';
import Paper from '@material-ui/core/Paper/Paper';
import theme from '../../theme';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	root: {
		color: '#5A5A5A',
		fontWeight: 600,
		overflow: 'hidden',
	},
	value: {
		fontWeight: 600,
		textTransform: 'uppercase',
		'& > span': {
			fontSize: '0.5em',
		},
	},
	label: {
		fontWeight: 600,
		fontSize: 15,
	},
	icon: {
		width: 77,
		height: 74,
		float: 'left',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: theme.spacing(2),
		borderRadius: '14px',
		boxShadow: '-2px 2px 3px rgba(141, 168, 205, 0.24);',
		background: 'linear-gradient(168.37deg, #9AEB6C 0.8%, #2D783A 109.68%);',
	},
}));
const ParamBlock = ({ value, label, icon, color }) => {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<div className={classes.icon}>
				<img src={icon} alt={label} />
			</div>
			<Box display="flex" style={{ justifyContent: 'space-between' }} flexDirection="column">
				<Typography variant="subtitle2" className={classes.label}>
					{label}
				</Typography>
				<Typography variant="h5" className={classes.value}>
					{value}
				</Typography>
			</Box>
		</div>
	);
};

export default ParamBlock;
