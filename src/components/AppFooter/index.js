import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import { routes } from '../../routes';
import { FormattedMessage } from 'react-intl';

import FormattedMessageFixed from '../FormattedMessageFixed';
import YouTubeIcon from '@material-ui/icons/YouTube';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import Container from '@material-ui/core/Container';
import { DateTime } from 'luxon';
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
	root: {
		background: '#484848',
		color: '#fff',
		paddingTop: theme.spacing(4),
		paddingBottom: theme.spacing(6),
		'&  a': {
			color: 'inherit',
			textDecoration: 'none',
			'&.active': {
				color: theme.palette.primary.main,
			},
			'&:hover': {
				textDecoration: 'underline',
			},
		},
	},
	container: {
		display: 'flex',
		justifyContent: 'space-between',
		[theme.breakpoints.down('sm')]: {
			flexDirection: 'column',
		},
	},
	info: {
		fontSize: '0.9em',
		'&  a': {
			color: 'rgba(255,255,255,.75)',
		},
		[theme.breakpoints.down('sm')]: {
			marginBottom: theme.spacing(2),
		},
	},
	nav: {
		display: 'flex',
		justifyContent: 'space-between',
		marginBottom: theme.spacing(4),
		'& > *:not(:first-child)': {
			marginLeft: theme.spacing(2),
		},
	},
	socials: {
		display: 'none',
		'& > *:not(:first-child)': {
			marginLeft: theme.spacing(2),
		},
	},
	copy: {
		display: 'block',
		paddingRight: 16,
		[theme.breakpoints.up('sm')]: {
			display: 'inline'
		},
	}
}));

const AppFooter = ({ maxWidth }) => {
	const classes = useStyles();
	const activeRoute = useSelector(({ router }) => router.location.pathname);
	return (
		<div className={classes.root}>
			<Container maxWidth={maxWidth} className={classes.container}>
				<div className={classes.wrapper}>
					<nav className={classes.nav}>
						{routes.map(({ name, path, defaultMessage, component }) =>
							component ? (
								<Link to={path} key={name} className={activeRoute === path ? 'active': null}>
									<FormattedMessageFixed
										id={`Routes.${name}`}
										description="route name"
										defaultMessage={defaultMessage}
									/>
								</Link>
							) : (
								<a href={path} key={name} target="_blank">
									<FormattedMessageFixed
										id={`Routes.${name}`}
										description="route name"
										defaultMessage={defaultMessage}
									/>
								</a>
							),
						)}
					</nav>
					<div className={classes.info}>
						<span className={classes.copy}>&copy; GEG.Finance {DateTime.local().year}</span>
						<a target="_blank" href={'https://app.geg.finance/terms.pdf'}>
							<FormattedMessage id={`Footer.Terms`} description="term link" defaultMessage={'Terms'} />
						</a>{' '}
						|{' '}
						<a target="_blank" href={'mailto:support@geg.finance'}>
							support@geg.finance
						</a>
					</div>
				</div>
				<div className={classes.socials}>
					<FacebookIcon />
					<TwitterIcon />
					<YouTubeIcon />
					<div
						style={{ textAlign: 'right', color: 'rgba(255,255,255,.5)', marginTop: 20, fontSize: '0.85em' }}
					>
						v{APP_VERSION}
					</div>
				</div>
			</Container>
		</div>
	);
};

export default AppFooter;
