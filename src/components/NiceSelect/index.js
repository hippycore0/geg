import React from 'react';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	root: {
		'& .MuiOutlinedInput-input': {
			// default 18.5px 14px
			padding: '8px 59px 8px 15px',
			fontSize: '12px',
			lineHeight: '16px',
			textTransform: 'uppercase',
			backgroundColor: '#fff',
			borderRadius: 10,
		},
		'& .MuiOutlinedInput-root fieldset': {
			borderColor: 'rgba(0,0,0,0.12)',
		},
	},
}));

const NiceSelect = ({ onChange, value, children, ...props }) => {
	const classes = useStyles();
	return (
		<FormControl className={classes.root} variant="outlined" {...props}>
			<Select value={value} onChange={onChange}>
				{children}
			</Select>
		</FormControl>
	);
};

export default NiceSelect;
