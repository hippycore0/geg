import faqIcon from '../../icons/Faq';
import langIcon from '../../icons/Lang';
import bagIcon from '../../icons/Bag';
import newsIcon from '../../icons/News';
import packingIcon from '../../icons/Packing';
import tokenIcon from '../../icons/Token';

export default {
	faq: faqIcon,
	lang: langIcon,
	bag: bagIcon,
	news: newsIcon,
	packing: packingIcon,
	token: tokenIcon,
};
