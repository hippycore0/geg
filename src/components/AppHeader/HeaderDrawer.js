import React from 'react';
import { useSelector } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Drawer from '@material-ui/core/Drawer/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import { makeStyles } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MenuIcon from '@material-ui/icons/Menu';
import { routes } from '../../routes';
import RenderRoutes from './RenderRoutes';

const useStyles = makeStyles((theme) => ({
	menuButton: {
		[theme.breakpoints.up('md')]: {
			display: 'none',
		},
	},
	sidebar: {
		padding: '0px 16px',
		margin: '8px 0',
		'& a': {
			display: 'block',
			padding: '8px 0',
			'&[href^="http"]': {
				padding: '4px 0',
				marginTop: '16px',
			},
		},
	},
	drawerClose: {},
	drawerDivider: {
		backgroundColor: 'rgba(255,255,255,0.12)',
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'flex-start',
		...theme.mixins.toolbar,
	},
	paper: {
		width: 250,
		maxWidth: '100%',
		backgroundColor: '#484848',
		color: '#fff',
	},
}));

const HeaderDrawer = () => {
	const activeRoute = useSelector(({ router }) => router.location.pathname);
	const classes = useStyles();
	const [state, setState] = React.useState({ drawerOpen: false });
	const closeDrawer = () => setState({ ...state, drawerOpen: false });
	return (
		<>
			<IconButton
				edge="start"
				className={classes.menuButton}
				color="inherit"
				aria-label="menu"
				onClick={() => setState({ ...state, drawerOpen: true })}
			>
				<MenuIcon />
			</IconButton>
			<Drawer anchor={'right'} open={state.drawerOpen} classes={{ paper: classes.paper }} onClose={closeDrawer}>
				<div className={classes.drawerHeader}>
					<IconButton onClick={closeDrawer}>
						<ChevronRightIcon style={{ color: 'white' }} />
					</IconButton>
				</div>
				<Divider className={classes.drawerDivider} />
				<List className={classes.sidebar}>
					<RenderRoutes routes={routes.filter((r) => r.component)} onClick={closeDrawer} activeRoute={activeRoute} />
				</List>
				<Divider className={classes.drawerDivider} />
				<List className={classes.sidebar}>
					<RenderRoutes routes={routes.filter((r) => !r.component)} />
				</List>
			</Drawer>
		</>
	);
};

export default HeaderDrawer;
