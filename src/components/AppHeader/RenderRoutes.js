import { Link } from "react-router-dom";
import React from "react";
import ListItem from "@material-ui/core/ListItem";
import FormattedMessageFixed from "../FormattedMessageFixed";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
	navItem: {
		fontSize: 14,
		lineHeight: 1,
		whiteSpace: 'nowrap',
		'&:hover svg': {
			color: theme.palette.primary.main,
		},
	},
	navItemActiveDot: {
		fontSize: 1,
		position: 'absolute',
		right: '-6px',
		top: '-2px',
		display: 'block',
		background: theme.palette.primary.main,
		width: 4,
		height: 4,
		borderRadius: 100,
	},
	navItemIcon: {
		color: '#F7F7F7',
		width: 16,
		height: 16,
		marginRight: theme.spacing(1),
		[theme.breakpoints.down('md')]: {
			display: 'none',
		},
	},
	navLink: {
		textDecoration: `none`,
		color: 'inherit',
		marginLeft: theme.spacing(4),
		[theme.breakpoints.down('lg')]: {
			marginLeft: 0,
		},
		'&:first-child': {
			marginLeft: 0,
		},
	},
	navLinkBordered: {
		borderColor: theme.palette.primary.main,
		borderRadius: 47,
		borderWidth: 1,
		borderStyle: 'solid',
	},


}));

const HeaderNavItem = ({ name, path, activeRoute, icon: Icon, defaultMessage }) => {
	const classes = useStyles();
	return (
		<ListItem className={classes.navItem}>
			{Icon && <Icon className={classes.navItemIcon} />}
			<span style={{ position: 'relative' }}>
				{activeRoute === path && <div className={classes.navItemActiveDot} />}
				<FormattedMessageFixed id={`Routes.${name}`} description="route name" defaultMessage={defaultMessage} />
			</span>
		</ListItem>
	);
};

const RenderRoutes = ({ routes, onClick, activeRoute}) => {
	const classes = useStyles();
	return routes.map(({ path, name, component, ...routProps }) => {
		if (!component) {
			return (
				<a
					href={path}
					key={name}
					className={`${classes.navLink} ${classes.navLinkBordered}`}
					target="_blank"
				>
					<HeaderNavItem name={name} path={path} activeRoute={activeRoute} {...routProps} />
				</a>
			);
		} else {
			return (
				<Link to={path} key={name} onClick={() => onClick ? onClick(path) : null } className={classes.navLink}>
					<HeaderNavItem name={name} path={path} activeRoute={activeRoute} {...routProps} />
				</Link>
			);
		}
	})
};

export default RenderRoutes;
