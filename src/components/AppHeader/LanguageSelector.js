import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector, useDispatch } from 'react-redux';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import LangIcon from '../../icons/Lang';
import ButtonBase from '@material-ui/core/ButtonBase';
import Popover from '@material-ui/core/Popover';
import { setCookie } from '../../tools/cookieHelper';

import EN from '../../../assets/svg/flags/EN.svg';
import DE from '../../../assets/svg/flags/DE.svg';
import RU from '../../../assets/svg/flags/RU.svg';
import CN from '../../../assets/svg/flags/CN.svg';
import ES from '../../../assets/svg/flags/ES.svg';


import { Settings } from 'luxon';
const langs = {
	en: {
		title: 'Eng',
		icon: EN,
	},
	de: {
		title: 'Deu',
		icon: DE,
	},
	ru: {
		title: 'Rus',
		icon: RU,
	},
	ch: {
		title: 'Chn',
		icon: CN,
	},
	es: {
		title: 'Esp',
		icon: ES,
	},
};

const useStyles = makeStyles((theme) => ({
	root: {
		color: theme.palette.primary.main,
		fontWeight: 600,
		textTransform: 'uppercase',
		paddingRight: theme.spacing(2),
	},
	icon: {
		marginRight: theme.spacing(1),
	},
}));
const LanguageSelector = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const { activeLang } = useSelector((state) => state.lang);
	const [state, setState] = React.useState({ open: false });
	const handleChangeLang = (lang) => {
		dispatch({ type: 'SET_LANG', payload: lang });
		setCookie('GegSavedLang', lang);
		Settings.defaultLocale = lang;
		setState({ ...state, open: null });
	};
	const handleToggle = (open) => setState({ ...state, open });
	const id = state.open ? 'lang-popover' : undefined;
	return (
		<>
			<ButtonBase aria-describedby={id} className={classes.root} onClick={(e) => handleToggle(e.currentTarget)}>
				<LangIcon className={classes.icon} /> {langs[activeLang].title}
			</ButtonBase>
			<Popover
				id={id}
				anchorEl={state.open}
				open={Boolean(state.open)}
				onClose={() => handleToggle(null)}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'left',
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
			>
				<MenuList>
					{Object.keys(langs)
						.filter((l) => l !== activeLang)
						.map((l) => (
							<MenuItem key={l} value={l} onClick={() => handleChangeLang(l)}>
								<img
									src={langs[l].icon}
									width={15}
									style={{ verticalAlign: 'middle', borderRadius: 100, marginRight: 5 }}
								/>
								{langs[l].title.toUpperCase()}
							</MenuItem>
						))}
				</MenuList>
			</Popover>
		</>
	);
};

export default LanguageSelector;
