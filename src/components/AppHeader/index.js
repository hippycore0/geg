import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import WalletConnector from '../WalletConnector';
import LanguageSelector from './LanguageSelector';
import HeaderNavBar from './HeaderNavBar';
import HeaderDrawer from "./HeaderDrawer";
import GegLogo from '../../../assets/logo-text.png';

const useStyles = makeStyles((theme) => ({
	root: {
		backgroundColor: '#484848',
		color: '#fff',
		paddingTop: theme.spacing(1),
		paddingBottom: theme.spacing(1),
		'& .MuiFormControl-root': {
			marginRight: theme.spacing(1),
		},
	},
	logoBox: {
		'& a': {
			textDecoration: 'none',
			color: 'inherit',
		},
		[theme.breakpoints.down('sm')]: {
			flexGrow: 1,
		},
	},
	logoLink: {
		width: 49,
		height: 57,
		display: 'inline-block',
		backgroundImage: `url(${GegLogo})`,
		backgroundSize: 'contain',
		backgroundRepeat: 'no-repeat',
		marginRight: theme.spacing(2),
		[theme.breakpoints.down('xs')]: {
			width: 37,
			height: 42,
			marginRight: theme.spacing(1),
		},
	},
	titleLink: {
		display: 'flex',
		'& > img': {
			height: 57,
			[theme.breakpoints.down('xs')]: {
				height: 42,
			},
		},
	},
	connectorWrapper: {
		[theme.breakpoints.down('sm')]: {
			display: 'none',
		},
	}
}));

const AppHeader = ({ maxWidth = 'lg' }) => {
	const classes = useStyles();
	return (
		<AppBar position="sticky" className={classes.root} elevation={0} color="transparent">
			<Toolbar>
				<Box display="flex" alignItems="center" className={classes.logoBox}>
					<Link to="/" title="Geg finance" className={classes.titleLink}>
						<img src={GegLogo} />
					</Link>
				</Box>
				<HeaderNavBar />
				<Box display="flex" justifyContent="flex-end">
					<LanguageSelector />
					<div className={classes.connectorWrapper}>
					<WalletConnector />
					</div>
				</Box>
				<HeaderDrawer />
			</Toolbar>
		</AppBar>
	);
};

export default AppHeader;
