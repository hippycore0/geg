import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import { routes } from '../../routes';
import { useSelector } from 'react-redux';
import { FormattedMessage } from "react-intl";
import RenderRoutes from "./RenderRoutes";

const useStyles = makeStyles((theme) => ({
	root: {
		display: `flex`,
		paddingLeft: theme.spacing(6),
		// justifyContent: 'center',
		[theme.breakpoints.down('lg')]: {
			paddingLeft: theme.spacing(2),
		},
		[theme.breakpoints.up('md')]: {
			flexGrow: 1,
		},
	},
	navList: {
		display: `flex`,
		[theme.breakpoints.down('sm')]: {
			display: 'none',
		},
	},
}));


const HeaderNavBar = () => {
	const activeRoute = useSelector(({ router }) => router.location.pathname);
	const classes = useStyles();
	return (
		<>
		<div className={classes.root}>
			<List component="nav" className={classes.navList}>
				<RenderRoutes routes={routes}  activeRoute={activeRoute} />
			</List>
		</div>
		{false && [<FormattedMessage id="Routes.products" defaultMessage="Products" />, <FormattedMessage id="Routes.tokenSale" defaultMessage="Token Sale" />]}
		</>
	);
};

export default HeaderNavBar;
