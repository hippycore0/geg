import React from 'react';
import CookieConsent from 'react-cookie-consent';
import { FormattedMessage } from 'react-intl';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	root: {
		zIndex: '1110 !important',
		'& a': {
			color: 'inherit',
		},
	},
	button: {
		backgroundColor: `${theme.palette.primary.main}  !important`,
		color: 'white !important',
		borderRadius: '10px !important',
		fontSize: '16px !important',
	},
}));
const CookieApprove = () => {
	const classes = useStyles();
	return (
		<CookieConsent
			cookieName="GegCookieConsent"
			buttonText="Agree"
			containerClasses={classes.root}
			buttonClasses={classes.button}
		>
			<FormattedMessage
				id="CookieApprove.message"
				defaultMessage="This site uses cookies for analytics, personalization and advertising. By continuing to browse it, you agree to our use of cookies. To find out more, "
			/>{' '}
			<a href="https://app.geg.finance/policy.pdf" target="_blank">
				<FormattedMessage id="CookieApprove.linkMore" defaultMessage="click here" />
			</a>
		</CookieConsent>
	);
};

export default CookieApprove;
