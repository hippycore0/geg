import React from 'react';
import { makeStyles } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import MetamaskIcon from '../../../assets/vendors/wallets/metamask.png';
import WalletConnectIcon from '../../../assets/vendors/wallets/wallectconnect.png';
import WalletConnectProvider from "../walletConnectProvider";
import TrustWalletIcon from '../../../assets/vendors/wallets/trustwallet.png';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		'& .MuiDialog-paper': {
			maxWidth: 420,
		},
		'&  .label-link': {
			color: theme.palette.primary.main,
		},
	},
	spacer: {
		marginBottom: theme.spacing(2),
	},
	connectBtn: {
		'& .MuiButton-label': {
			width: 140,
			justifyContent: 'flex-start',
		},
	},
}));

const TermsCheckbox = () => (
	<>
		<FormattedMessage id="TermsCheckbox.message" defaultMessage="I accept" />{' '}
		<a href="https://app.geg.finance/terms.pdf" target="_blank" className="label-link">
			<FormattedMessage id="TermsCheckbox.termLink" defaultMessage="Terms of Use" />
		</a>{' '}
		<FormattedMessage id="TermsCheckbox.betweenLinks" defaultMessage="&" />{' '}
		<a href="https://app.geg.finance/policy.pdf" target="_blank" className="label-link">
			<FormattedMessage id="TermsCheckbox.privacyLink" defaultMessage="Privacy Policy" />
		</a>
	</>
);

const WalletConnectorDialog = ({ isOpen, onClose }) => {
	const classes = useStyles();
	const walletConnecting = useSelector((state) => state.loading.indexOf('GET_ACCOUNT') >= 0);
	const dispatch = useDispatch();
	const [checked, setChecked] = React.useState(false);
	const handleChange = (event) => {
		setChecked(event.target.checked);
	};
	return (
		<Dialog open={isOpen} onClose={onClose} className={classes.root}>
			<DialogTitle>
				<Box display="flex" alignItems="center">
					<Box flexGrow={1}>
						<FormattedMessage id="WalletConnectorDialog.header" defaultMessage="Connect your wallet" />
					</Box>
					<Box>
						<IconButton onClick={onClose}>
							<CloseIcon />
						</IconButton>
					</Box>
				</Box>
			</DialogTitle>
			<DialogContent>
				<div className={classes.spacer}>
					{walletConnecting && (
						<Box style={{ display: 'flex', alignItems: 'center' }}>
							<CircularProgress size={25} style={{ marginRight: 8 }} />{' '}
							<FormattedMessage id="WalletConnectorDialog.inProgress" defaultMessage="Initializing..." />
						</Box>
					)}
					{!walletConnecting && (
						<FormGroup row>
							<FormControlLabel
								label={<TermsCheckbox />}
								control={
									<Checkbox
										checked={checked}
										onChange={handleChange}
										inputProps={{ 'aria-label': 'primary checkbox' }}
									/>
								}
							/>
						</FormGroup>
					)}
				</div>
				<div className={classes.spacer}>
					<Button
						fullWidth
						className={classes.connectBtn}
						variant="outlined"
						disabled={!checked || walletConnecting}
						onClick={() => dispatch({ type: 'GET_ACCOUNT' })}
						startIcon={<img src={MetamaskIcon} />}
					>
						Metamask
					</Button>
				</div>
				<div className={classes.spacer}>
					<WalletConnectProvider>
						{({ fetching, onClick } ) => (
							<Button
								fullWidth
								className={classes.connectBtn}
								variant="outlined"
								disabled={!checked || fetching}
								onClick={onClick}
								startIcon={<img src={WalletConnectIcon} />}
							>
								WalletConnect
							</Button>
						)}
					</WalletConnectProvider>
				</div>
				{/*{!walletConnecting && (*/}
				{/*	<div className={classes.spacer}>*/}
				{/*		<Button*/}
				{/*			fullWidth*/}
				{/*			className={classes.connectBtn}*/}
				{/*			variant="outlined"*/}
				{/*			disabled*/}
				{/*			onClick={() => dispatch({ type: 'GET_ACCOUNT' })}*/}
				{/*			startIcon={<img src={TrustWalletIcon} />}*/}
				{/*		>*/}
				{/*			TrustWallet*/}
				{/*		</Button>*/}
				{/*	</div>*/}
				{/*)}*/}
			</DialogContent>
		</Dialog>
	);
};

export default WalletConnectorDialog;
