import React from 'react';
import Jazzicon, { jsNumberForAddress } from 'react-jazzicon';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core';
import WalletConnectorDialog from './WallectConnectorDialog';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		paddingTop: theme.spacing(4),
	},
	spacer: { marginBottom: theme.spacing(4) },
	connectBtn: {
		color: '#FFF',
		borderColor: 'rgba(0,0,0,0.12)',
		borderRadius: 47,
		fontWeight: 600,
	},
	balanceBtn: {
		paddingRight: '22px',
		whiteSpace: 'nowrap',
	},
	accountBtn: {
		marginLeft: '-15px',
		backgroundColor: 'white',
		'&:hover': {
			backgroundColor: '#ececec',
		},
	},
	walletIcon: {
		verticalAlign: 'middle',
		marginRight: theme.spacing(1),
	},
}));
const WalletConnector = () => {
	const classes = useStyles();
	const { account, balance } = useSelector((state) => state.wallet);
	const accountLoading = useSelector((state) => state.loading.indexOf('GET_ACCOUNT') >= 0);
	const [dialogIsOpen, setDialogIsOpen] = React.useState(false);
	const dispatch = useDispatch();

	React.useEffect(() => {
		if (account) {
			dispatch({ type: 'GET_BALANCE', payload: account });
		}
	}, [account]);

	if (account) {
		return (
			<Box display="flex" alignItems="center" justifyContent="center">
				<Button
					variant="contained"
					className={classes.balanceBtn}
					size="small"
					disableElevation
					color="primary"
				>
					{Math.round(balance * 10000) / 10000} ETH
				</Button>
				<Button
					size="small"
					disableElevation
					startIcon={<Jazzicon diameter={20} seed={jsNumberForAddress(account)} />}
					className={classes.accountBtn}
					href={`https://etherscan.io/address/${account}`}
					variant="outlined"
					target="_blank"
				>
					{account.slice(0, 6)}...{account.slice(-4)}
				</Button>
				{/*<WalletConnectorDialog isOpen={dialogIsOpen} onClose={() => setDialogIsOpen(false)} />*/}
			</Box>
		);
	}

	return (
		<>
			<WalletConnectorDialog isOpen={dialogIsOpen} onClose={() => setDialogIsOpen(false)} />
			{accountLoading && (
				<Box display="flex" alignItems="center" justifyContent="center">
					<CircularProgress size={20} style={{ marginRight: 8 }} />{' '}
					<FormattedMessage id="WalletConnector.connectionInProgress" defaultMessage="Connecting..." />
				</Box>
			)}
			{!accountLoading && (
				<Button
					variant="contained"
					color="primary"
					size="small"
					className={classes.connectBtn}
					onClick={() => setDialogIsOpen(true)}
				>
					<FormattedMessage id="WalletConnector.btn" defaultMessage="Connect wallet" />
				</Button>
			)}
		</>
	);
};

export default WalletConnector;
