import React from 'react';
import { makeStyles } from '@material-ui/core';
import WalletConnector from "./index";
const useStyles = makeStyles((theme) => ({
	root: {
		position: 'fixed',
		display: 'flex',
		justifyContent: 'center',
		bottom: 0,
		left: 0,
		right: 0,
		zIndex: 10,
		padding: theme.spacing(2),
		backgroundColor: '#484848',
		color: 'white',
		[theme.breakpoints.up('sm')]: {
			display: 'none'
		}
	},
}));

const WalletConnectorMobile = () => {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<WalletConnector/>
		</div>
	);
};
export default WalletConnectorMobile;
