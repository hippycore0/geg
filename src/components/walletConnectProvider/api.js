const ethApiRequest = async (url) => {
	const baseURL = 'https://ethereum-api.xyz';
	const options = {
		method: 'GET',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
		},
	};
	const response = await fetch(`${baseURL}${url}`, options);
	if (response.status >= 400) {
		throw new Error(response.statusText);
	}
	return await response.json();
};

export const apiGetAccountAssets = async (address, chainId) => {
	const response = await ethApiRequest(`/account-assets?address=${address}&chainId=${chainId}`);
	const { result } = response.data;
	return result;
};

export const apiGetAccountNonce = async (address, chainId) => {
	const response = await ethApiRequest(`/account-nonce?address=${address}&chainId=${chainId}`);
	const { result } = response.data;
	return result;
};

export const apiGetGasPrices = async () => {
	const response = await ethApiRequest(`/gas-prices`);
	const { result } = response.data;
	return result;
};

export const apiGetAccountTransactions = async (address, chainId) => {
	const response = await ethApiRequest(`/account-transactions?address=${address}&chainId=${chainId}`);
	const { result } = response.data;
	return result;
};
