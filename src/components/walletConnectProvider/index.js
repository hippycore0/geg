import React from 'react';
import { useDispatch } from 'react-redux';
import Button from '@material-ui/core/Button';

const WalletConnectProvider = ({ children }) => {
	const dispatch = useDispatch();
	return (
		<>
			{children({ onClick: () => dispatch({ type: 'WALLET_CONNECT_INIT' }), fetching: false })}
			{MODE === 'development' && (<Button onClick={() => dispatch({ type: 'WALLET_CONNECT_KILL'})}>
				disconnect
			</Button>)}
		</>
	);
};

export default WalletConnectProvider;
