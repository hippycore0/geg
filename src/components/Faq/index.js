import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import BlockHeader from '../../components/BlockHeader';
import Loader from '../Loader';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		marginBottom: theme.spacing(4),
	},
	heading: {
		fontSize: theme.typography.pxToRem(15),
		fontWeight: theme.typography.fontWeightBold,
	},
}));

const Faq = ({ tag, header }) => {
	const classes = useStyles();
	const data = useSelector((state) =>
		state.faq.entries.filter((topic) => topic.tag === tag).reduce((a, b) => a.concat(b.questions), []).sort((a, b) => b.sort_order - a.sort_order),
	);
	const resourcesIsLoading = useSelector((state) => state.loading.indexOf('FETCH_FAQ') >= 0);

	if ((!data || !data.length) && !resourcesIsLoading) {
		return null;
	}

	return (
		<>
			{header || <BlockHeader>FAQ</BlockHeader>}
			<div className={classes.root}>
				{resourcesIsLoading && <Loader />}
				{!resourcesIsLoading &&
					data.map(({ question, answer }, i) => {
						return (
							<Accordion key={i}>
								<AccordionSummary
									expandIcon={<ExpandMoreIcon />}
									aria-controls="panel1a-content"
									id="panel1a-header"
								>
									<Typography className={classes.heading}>{question}</Typography>
								</AccordionSummary>
								<AccordionDetails>
									<Typography dangerouslySetInnerHTML={{__html: answer}} />
								</AccordionDetails>
							</Accordion>
						);
					})}
			</div>
		</>
	);
};

export default Faq;
