import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import theme from '../../theme';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
		alignItems: 'center',
		[theme.breakpoints.down('sm')]: {
			flexDirection: 'column',
			alignItems: 'flex-start',
			'& > h6': {
				marginBottom: theme.spacing(2),
			},
		},
	},
}));

const BlockHeader = ({ children, controls = false, subHeader, ...props }) => {
	const classes = useStyles();
	return (
		<div style={{ marginBottom: theme.spacing(2) }}>
			<Box className={classes.root}>
				{children && (
					<Typography variant="h6" {...props}>
						{children}
					</Typography>
				)}
				{controls && (
					<Grid item style={{ marginLeft: theme.spacing(2) }}>
						{controls}
					</Grid>
				)}
			</Box>
			{subHeader && (
				<Typography variant="subtitle2" style={{ color: '#777' }}>
					{subHeader}
				</Typography>
			)}
		</div>
	);
};

export default BlockHeader;
