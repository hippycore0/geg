import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
	root: {
		background: 'linear-gradient(180deg, #89D75D 0%, #76C948 23.2%, #77CA49 70.6%, #89D75D 100%);',
		boxShadow: '1px 1px 3px rgba(31, 138, 52, 0.4)',
		color: '#FFFFFF',
		'&:disabled': {
			background: '#ECECEC',
			boxShadow: '1px 1px 4px rgba(0, 0, 0, 0.1)',
			borderColor: 'transparent',
		},
		'&:active': {
			background: '#A1D982',
			boxShadow: 'inset 1px 1px 5px rgba(31, 138, 52, 0.6)',
		},
		'&.MuiButton-outlined': {
			background: '#ECECEC',
			boxShadow: '1px 1px 4px rgba(0, 0, 0, 0.1)',
			color: '#989898',
			border: 'none',
			'&:active': {
				boxShadow: 'inset 1px 1px 5px rgba(0, 0, 0, 0.5)',
			},
		},
	},
}));

export default function GradientBtn(props) {
	const { color, ...other } = props;
	const classes = useStyles(props);
	return <Button className={classes.root} {...other} />;
}
