import React from 'react';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core';
import { FormattedMessage } from "react-intl";

const useStyles = makeStyles((theme) => ({
	root: {
		paddingTop: theme.spacing(4),
	},
	spacer: { marginBottom: theme.spacing(4) },
}));

const NoMatch = () => {
	const classes = useStyles();
	return (
		<Container className={classes.root}>
			<Typography variant="h5" className={classes.spacer}>
				404
			</Typography>
			<p><FormattedMessage id="NoMatch.message" defaultMessage="Page not found" /></p>
		</Container>
	);
};

export default NoMatch;
