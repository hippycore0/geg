import React from 'react';
import Faq from '../../components/Faq';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core';
import NavIcons from '../../components/AppHeader/NavIcons';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		paddingTop: theme.spacing(4),
	},
	header: {
		marginBottom: theme.spacing(4),
		flexGrow: 1,
		textTransform: 'uppercase',
		'& > svg': { color: theme.palette.primary.main, position: 'relative', top: '2px' },
	},
}));

const Help = () => {
	const classes = useStyles();
	return (
		<Container className={classes.root}>
			<Faq
				tag="Help"
				header={
					<Typography variant="h5" className={classes.header}>
						<NavIcons.faq /> <FormattedMessage id="Routes.faq" defaultMessage="FAQ" />
					</Typography>
				}
			/>
		</Container>
	);
};

export default Help;
