import React from 'react';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core';
import NavIcons from '../../components/AppHeader/NavIcons';

const useStyles = makeStyles((theme) => ({
	root: {
		paddingTop: theme.spacing(4),
	},
	spacer: { marginBottom: theme.spacing(4) },
}));

const News = () => {
	const classes = useStyles();
	return (
		<Container className={classes.root}>
			<Typography variant="h5" className={classes.spacer}>
				<NavIcons.news /> News
			</Typography>
		</Container>
	);
};

export default News;
