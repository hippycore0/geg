import React from 'react';
import { Route, Switch } from 'react-router';
import Dashboard from './Dashboard';
import Finance from './Finance';
import News from './News';
import Help from './Help';
import NoMatch from './NoMatch';
import NavIcons from '../components/AppHeader/NavIcons';

export const routes = [
	{ path: '/', icon: NavIcons.packing, name: 'products', sort: 0, defaultMessage: 'Products', component: Dashboard },
	{
		path: '/finance',
		icon: NavIcons.bag,
		name: 'finance',
		sort: 1,
		defaultMessage: 'My finance',
		component: Finance,
	},
	// { path: '/news', icon: NavIcons.news, name: 'news', sort: 2, component: News, defaultMessage: 'News' },
	{ path: '/faq', icon: NavIcons.faq, name: 'faq', sort: 3, component: Help, defaultMessage: 'FAQ' },
	{
		path: 'https://tokensale.geg.finance/',
		icon: NavIcons.token,
		sort: 4,
		name: 'tokenSale',
		defaultMessage: 'Token Sale',
	},
];

export default () => (
	<Switch>
		<Route exact path="/" component={Dashboard} />
		<Route path="/finance" component={Finance} />
		<Route path="/news" component={News} />
		<Route path="/faq" component={Help} />
		<Route path="*" component={NoMatch} />
	</Switch>
);
