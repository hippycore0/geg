import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import numeral from 'numeral';
import Grid from '@material-ui/core/Grid';
import { red, blue, yellow } from '@material-ui/core/colors';
import ParamBlock from '../../components/ParamBlock';
import ParamIconMoney from '../../../assets/svg/param-icon-money.svg';
import ParamIconStar from '../../../assets/svg/param-icon-star.svg';
import ParamIconLock from '../../../assets/svg/param-icon-loocker.svg';
import ParamIconSpeed from '../../../assets/svg/param-icon-speed.svg';

import theme from '../../theme';
import { FormattedMessage } from 'react-intl';
const StyledParamsWrapper = styled(Grid)`
	margin-bottom: ${theme.spacing(6)}px;
`;

const maxInterestRateSelector = (state) => {
	if(!state.products.entries.length) {
		return 0;
	}
	const filtered = state.products.entries.filter(product => {
		const relToken = state.tokens.entries.find((t) => t.uuid === product.token_product);
		return relToken && relToken.network.code === state.wallet.network;
	});
	if(!filtered.length) {
		return 0
	}
	return filtered.sort((a, b) => parseFloat(b.apy) - parseFloat(a.apy))[0].apy;
};

const ParamsBlocks = () => {
	const { valueAllocated, openValue, maxVolume, maxInterestRate } = useSelector((state) => ({
		valueAllocated: numeral(state.products.totals.value_allocated_usdt).format('0a'),
		openValue: numeral(Math.round(Math.round(state.products.totals.open_volume_usdt) / 1000)).format() + 'K',
		maxVolume: numeral(Math.round(Math.round(state.products.totals.max_volume_usdt) / 1000)).format() + 'K',
		maxInterestRate: maxInterestRateSelector(state),
	}));

	return (
		<StyledParamsWrapper container spacing={2}>
			<Grid item xs={12} md={3} lg={3}>
				<FormattedMessage id="ParamsBlocks.allocated" defaultMessage="Total value allocated">
					{(msg) => (
						<ParamBlock
							label={msg}
							value={
								<>
									{valueAllocated}
									<span>USD</span>
								</>
							}
							icon={ParamIconMoney}
						/>
					)}
				</FormattedMessage>
			</Grid>

			<Grid item xs={12} md={3} lg={3}>
				<FormattedMessage id="ParamsBlocks.maxInterest" defaultMessage="Maximum yield (APY)">
					{(msg) => (
						<ParamBlock
							label={msg}
							value={
								<>
									{Math.round(maxInterestRate)}
									<span>
										{maxInterestRate.toString().slice(maxInterestRate.toString().indexOf('.'))}%
									</span>
								</>
							}
							icon={ParamIconStar}
							color={yellow[700]}
						/>
					)}
				</FormattedMessage>
			</Grid>

			<Grid item xs={12} md={3} lg={3}>
				<FormattedMessage id="ParamsBlocks.openVolume" defaultMessage="Open volume for deposits">
					{(msg) => (
						<ParamBlock
							label={msg}
							value={
								<>
									{openValue}
									<span>USD</span>
								</>
							}
							icon={ParamIconLock}
							color={red[500]}
						/>
					)}
				</FormattedMessage>
			</Grid>

			<Grid item xs={12} md={3} lg={3}>
				<FormattedMessage id="ParamsBlocks.maxVolume" defaultMessage="Max volume for deposits">
					{(msg) => (
						<ParamBlock
							label={msg}
							value={
								<>
									{maxVolume}
									<span>USD</span>
								</>
							}
							icon={ParamIconSpeed}
							color={blue[500]}
						/>
					)}
				</FormattedMessage>
			</Grid>
		</StyledParamsWrapper>
	);
};

export default ParamsBlocks;
