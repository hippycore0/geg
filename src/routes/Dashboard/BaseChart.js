import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import styled from 'styled-components';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Paper from '@material-ui/core/Paper/Paper';
import BlockHeader from '../../components/BlockHeader';
import theme from '../../theme';

const HighchartsWrapper = styled.div`
	.highcharts-credits {
		display: none;
	}
`;

const options = {
	chart: {
		type: 'areaspline',
	},
	series: [
		{
			name: 'GEG',
			data: [3, 4, 3, 5, 4, 10, 12],
		},
		{
			name: 'USDT',
			data: [1, 3, 4, 3, 3, 5, 4],
		},
	],
	colors: [theme.palette.secondary.main, theme.palette.primary.main],
	plotOptions: {
		areaspline: {
			fillOpacity: 0.5,
		},
	},
	yAxis: {
		title: null,
	},
	xAxis: {
		categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
	},
	title: null,
};

const ChartSwitchBtnWrapper = styled(ButtonGroup)`
	display: flex;
	justify-content: center;
`;

const BaseChart = () => {
	const [chartState, setChartState] = React.useState(0);
	return (
		<>
			<BlockHeader>Analytics</BlockHeader>
			<Paper className="paper">
				<Box>
					<HighchartsWrapper>
						<HighchartsReact highcharts={Highcharts} options={options} />
					</HighchartsWrapper>
					<ChartSwitchBtnWrapper color="primary">
						<Button
							size="small"
							color="primary"
							variant={chartState === 0 ? 'contained' : 'outlined'}
							onClick={() => setChartState(0)}
						>
							Total invested
						</Button>
						<Button
							size="small"
							color="primary"
							variant={chartState === 1 ? 'contained' : 'outlined'}
							onClick={() => setChartState(1)}
						>
							Transaction
						</Button>
					</ChartSwitchBtnWrapper>
				</Box>
			</Paper>
		</>
	);
};

export default BaseChart;
