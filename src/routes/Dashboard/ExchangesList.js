import React from 'react';
import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import { green, red } from '@material-ui/core/colors';
import { useSelector } from 'react-redux';
import theme from '../../theme';
import BlockHeader from '../../components/BlockHeader';
const ListWrapper = styled.div``;

const ListEntry = styled(Paper)`
	padding: ${theme.spacing(2)}px;
	margin-bottom: ${theme.spacing(2)}px;
`;

const ExchangesList = () => {
	const exchanges = useSelector((state) => state.exchanges.entries);
	return (
		<>
			<BlockHeader>Buy Token GEG</BlockHeader>
			<ListWrapper>
				{exchanges.map((exchange) => (
					<ListEntry key={exchange.name}>
						<Grid container justify="space-between" alignItems="center">
							<div style={{ display: 'flex', alignItems: 'center' }}>
								<img src={exchange.logo} style={{ marginRight: 8, width: 30 }} />
								<Typography variant="h5">{exchange.name}</Typography>
							</div>
							{exchange.price && (
								<div
									style={{
										flexGrow: 1,
										display: 'flex',
										alignItems: 'center',
										justifyContent: 'flex-end',
									}}
								>
									<Typography variant="h5" style={{ marginRight: '0.5em' }}>
										{exchange.price}$
									</Typography>
									{exchange.positive ? (
										<ArrowUpwardIcon style={{ color: green[500] }} />
									) : (
										<ArrowDownwardIcon style={{ color: red[500] }} />
									)}
								</div>
							)}
							<Button
								component="a"
								variant="contained"
								color="primary"
								size="small"
								href={exchange.url}
								target="_blank"
							>
								Buy
							</Button>
						</Grid>
					</ListEntry>
				))}
			</ListWrapper>
		</>
	);
};

export default ExchangesList;
