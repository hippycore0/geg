import React from 'react';
import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { FormattedMessage } from 'react-intl';
import ParamsBlocks from './ParamsBlocks';
import ProductsList from '../../components/Products/ProductsList';
import theme from '../../theme';

const DashboardStyled = styled.div`
	margin-top: ${theme.spacing(4)}px;
	.paper {
		padding: ${theme.spacing(2)}px;
	}
	.paper-header {
		margin-bottom: ${theme.spacing(1)}px;
	}
`;

const Dashboard = () => {
	return (
		<DashboardStyled>
			<Container>
				<Typography variant="h5" style={{ marginBottom: theme.spacing(4), textAlign: 'center' }}>
					<FormattedMessage
						id="Dashboard.header"
						defaultMessage="INDUSTRIAL SCALE PROFITABLE BUSINESS FINANCING IN GREEN ENERGY SECTOR"
					/>
				</Typography>
				<ParamsBlocks />
			</Container>
			<ProductsList />
		</DashboardStyled>
	);
};

export default Dashboard;
