import React from 'react';
import { makeStyles } from '@material-ui/core';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Table from '@material-ui/core/Table';
import DepositTableRow from './DepositTableRow';
import InfoIcon from '@material-ui/icons/InfoOutlined';
import HelpPopup from '../../components/HelpPopup';
import { FormattedMessage } from 'react-intl';

const useRowStyles = makeStyles({
	root: {
		'& .MuiTableBody-root > .MuiTableRow-root:last-child > .MuiTableCell-root': {
			borderBottom: 'none',
		},
	},
	infoIcon: {
		color: '#888',
		fontSize: 16,
		verticalAlign: 'middle',
		cursor: 'pointer',
	},
});
const DepositTable = ({ product }) => {
	const classes = useRowStyles();
	const [infoDialogOpen, setInfoDialogOpen] = React.useState(false);
	return (
		<>
			<FormattedMessage
				id="DepositTable.helpText"
				defaultMessage="Deposit yield is accrued each 24 hours starting the time of deposit allocation. Accumulated yield is shown in the currency of deposit and in GEG in accordance with current exchange rate. The exact amount of accumulated yield in GEG (reward) will be determined upon withdrawal."
			>
				{(msg) => <HelpPopup text={msg} open={infoDialogOpen} onClose={() => setInfoDialogOpen(false)} />}
			</FormattedMessage>
			<Table size="small" className={classes.root}>
				<TableHead>
					<TableRow>
						{/*<TableCell size="small" variant="head">*/}
						{/*	ID*/}
						{/*</TableCell>*/}
						<TableCell size="small" variant="head" width={180}>
							<FormattedMessage id="DepositTable.cellTx" defaultMessage="Tx" />
						</TableCell>
						<TableCell size="small" variant="head" align="right" width={115}>
							<FormattedMessage id="DepositTable.cellStart" defaultMessage="Start date" />
						</TableCell>
						<TableCell size="small" variant="head" align="right" width={115}>
							<FormattedMessage id="DepositTable.cellEnd" defaultMessage="End date" />
						</TableCell>
						<TableCell size="small" variant="head" align="right" width={200}>
							<FormattedMessage id="DepositTable.cellLockedUntil" defaultMessage="Yield redemption date" />
						</TableCell>
						{product.days > 1 && (
							<TableCell size="small" variant="head">
								<FormattedMessage id="DepositTable.cellRenewal" defaultMessage="Auto-renewal" />
							</TableCell>
						)}
						<TableCell size="small" variant="head" align="right" width={200}>
							<FormattedMessage id="DepositTable.cellDeposit" defaultMessage="Deposit" />
						</TableCell>
						<TableCell
							size="small"
							variant="head"
							align="right"
							colSpan={['GEG', 'GRG'].indexOf(product.token_product_symbol) < 0 ? 2 : 1}
						>
							<FormattedMessage id="DepositTable.cellAccum" defaultMessage="Accumulated yield" />{' '}
							<InfoIcon onClick={() => setInfoDialogOpen(true)} className={classes.infoIcon} />
						</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{product.deposits.map((deposit) => (
						<DepositTableRow
							key={`${deposit.product_id}_${deposit.deposit_id}`}
							deposit={deposit}
							product={product}
						/>
					))}
				</TableBody>
			</Table>
		</>
	);
};

export default DepositTable;
