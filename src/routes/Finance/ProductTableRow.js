import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import DepositTable from './DepositTable';
import EtherscanLink from '../../components/EtherscanLink';
import classNames from 'classnames';

const useStyles = makeStyles((theme) => ({
	root: {
		// '&:hover': {
		// 	backgroundColor: '#ededed',
		// }
	},
	openRow: {
		backgroundColor: '#f0f0f0',
	},
	tokenProductLogo: {
		verticalAlign: 'middle',
	},
	paddingCell: {
		[theme.breakpoints.down('md')]: {
			display: 'none',
		},
	},
}));

const ProductTableRow = (props) => {
	const { product } = props;
	const [open, setOpen] = React.useState(false);
	const classes = useStyles();
	return (
		<React.Fragment>
			<TableRow className={classNames([classes.root, open ? classes.openRow : null])}>
				<TableCell width={1}>
					<IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
						{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
					</IconButton>
				</TableCell>
				<TableCell>
					<img
						height={26}
						src={product.logo}
						className={classes.tokenProductLogo}
						alt={product.token_product_symbol}
					/>{' '}
					<EtherscanLink hash={product.contract_id}>{product.name}</EtherscanLink>
				</TableCell>
				<TableCell align="right">{product.apy}%</TableCell>
				<TableCell align="right">{product.period}</TableCell>
				<TableCell align="right">{product.payout_interval} days</TableCell>
				{/*<TableCell align="right">*/}
				{/*	{product.pool_volume_token} {product.token_product_symbol}*/}
				{/*</TableCell>*/}
			</TableRow>
			<TableRow>
				<TableCell style={{ paddingBottom: 0, paddingTop: 0 }} className={classes.paddingCell} />
				<TableCell style={{ paddingBottom: 0, paddingTop: 0, paddingRight: 0 }} colSpan={10}>
					<Collapse in={open} timeout="auto" unmountOnExit>
						<DepositTable product={product} />
					</Collapse>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
};

ProductTableRow.propTypes = {
	product: PropTypes.shape({}).isRequired,
};
export default ProductTableRow;
