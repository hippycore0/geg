import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';
import React from 'react';
import styled from 'styled-components';

const HighchartsWrapper = styled.div`
	.highcharts-credits {
		display: none;
	}
`;
const options = {
	chart: {
		//	renderTo: (options.chart && options.chart.renderTo) || (hasRenderToArg && a),
		backgroundColor: null,
		borderWidth: 0,
		type: 'area',
		margin: [2, 0, 2, 0],
		width: 120,
		height: 20,
		style: {
			overflow: 'visible',
		},
		// small optimalization, saves 1-2 ms each sparkline
		skipClone: true,
	},
	series: [
		{
			name: 'GEG',
			data: [3, 4, 3, 5, 4, 10, 12],
		},
	],
	title: {
		text: '',
	},
	credits: {
		enabled: false,
	},
	xAxis: {
		labels: {
			enabled: false,
		},
		title: {
			text: null,
		},
		startOnTick: false,
		endOnTick: false,
		tickPositions: [],
	},
	yAxis: {
		endOnTick: false,
		startOnTick: false,
		labels: {
			enabled: false,
		},
		title: {
			text: null,
		},
		tickPositions: [0],
	},
	legend: {
		enabled: false,
	},
	tooltip: {
		hideDelay: 0,
		outside: true,
		shared: true,
	},
	plotOptions: {
		series: {
			animation: false,
			lineWidth: 1,
			shadow: false,
			states: {
				hover: {
					lineWidth: 1,
				},
			},
			marker: {
				radius: 1,
				states: {
					hover: {
						radius: 2,
					},
				},
			},
			fillOpacity: 0.25,
		},
		column: {
			negativeColor: '#910000',
			borderColor: 'silver',
		},
	},
};

const SparkLineChart = () => {
	return (
		<HighchartsWrapper>
			<HighchartsReact highcharts={Highcharts} options={options} />
		</HighchartsWrapper>
	);
};

export default SparkLineChart;
