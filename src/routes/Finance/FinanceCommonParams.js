import React from 'react';
import { useSelector } from 'react-redux';
import numeral from 'numeral';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		marginBottom: theme.spacing(2),
		flexGrow: 1,
	},
	block: {
		fontWeight: 600,
		whiteSpace: 'nowrap',
		padding: theme.spacing(2),
	},
	label: {
		fontWeight: 500,
		color: '#9d9d9d',
		fontSize: '0.95em',
	},
	value: {
		fontWeight: 500,
		whiteSpace: 'nowrap',
		fontSize: '1.85em',
	},
	sum: {
		fontWeight: 500,
		fontSize: '1em',
		color: '#727272',
	},
}));

const FinanceCommonParams = () => {
	const classes = useStyles();
	const { totals } = useSelector((state) => state.deposits);
	// console.log('TOTLAS', totals);
	return (
		<Paper className={classes.root}>
			<Grid container spacing={4}>
				<Grid item xs={12} md={4} lg={4}>
					<div className={classes.block}>
						<Typography className={classes.label}>
							<FormattedMessage id="FinanceCommonParams.allocated" defaultMessage="Allocated value" />
						</Typography>
						<Typography className={classes.value}>
							{numeral(totals.total_deposited_geg).format('0.[000000]')} GEG
						</Typography>
						<Typography className={classes.sum}>
							&#8776; ${numeral(totals.total_deposited_usdt).format('0,0.[000000]')}
						</Typography>
					</div>
				</Grid>
				<Grid item xs={12} md={4} lg={4}>
					<div className={classes.block}>
						<Typography className={classes.label}>
							<FormattedMessage id="FinanceCommonParams.accumulated" defaultMessage="Accumulated yield" />
						</Typography>
						<Typography className={classes.value}>
							{numeral(totals.total_interest_geg).format('0.[000000]')} GEG
						</Typography>
						<Typography className={classes.sum}>
							&#8776; ${numeral(totals.total_interest_usdt).format('0,0.[000000]')}
						</Typography>
					</div>
				</Grid>
				<Grid item xs={12} md={4} lg={4}>
					<div className={classes.block}>
						<Typography className={classes.label}>
							<FormattedMessage id="FinanceCommonParams.today" defaultMessage="Yield for today" />
						</Typography>
						<Typography className={classes.value}>
							{numeral(totals.total_interest_today_geg).format('0.[000000]')} GEG
						</Typography>
						<Typography className={classes.sum}>
							&#8776; ${numeral(totals.total_interest_today_usdt).format('0,0.[000000]')}
						</Typography>
					</div>
				</Grid>
			</Grid>
		</Paper>
	);
};

export default FinanceCommonParams;
