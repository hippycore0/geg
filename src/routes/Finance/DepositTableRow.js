import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TableCell from '@material-ui/core/TableCell/TableCell';
import Button from '@material-ui/core/Button';
import TableRow from '@material-ui/core/TableRow';
import CircularProgress from '@material-ui/core/CircularProgress';
import { DateTime } from 'luxon';
import numeral from 'numeral';
import EtherscanLink from '../../components/EtherscanLink';
import { isLoading } from '../../redux/selectors';
import WithdrawalDialog from '../../components/ProductDialog/WithdrawalDialog';
import { FormattedMessage } from 'react-intl';
import Switch from "@material-ui/core/Switch/Switch";

const DepositTableRow = ({ product, deposit }) => {
	const dispatch = useDispatch();
	const { account, transactions, isLoadingWithdrawal, isLoadingClaim, depositAutoRenewalLoading } = useSelector((state) => ({
		account: state.wallet.account,
		transactions: state.transactions.filter(
			(tx) => tx.contract === product.contract_id && tx.depositId === deposit.deposit_id,
		),
		isLoadingWithdrawal: isLoading(state, { id: deposit.deposit_id, name: 'withdrawal' }),
		isLoadingClaim: isLoading(state, { id: deposit.deposit_id, name: 'claims' }),
		depositAutoRenewalLoading: state.deposits.autoRenewalLoaders.indexOf(`${product.contract_id}_${deposit.deposit_id}`) >= 0,
	}));

	const [withdrawDialogDepositId, setWithdrawDialogDepositId] = React.useState(null);
	const withdrawalTransaction = transactions.find((tx) => tx.type === 'withdrawal');
	const claimTransaction = transactions.find((tx) => tx.type === 'claim');
	const handleClickAction = (type, depositId) =>
		dispatch({
			type, // 'MAKE_WITHDRAWAL',  'MAKE_CLAIM'
			payload: {
				contractId: product.contract_id,
				from: account,
				depositId,
			},
		});
	const handleChangeAutoRenewal = (e) => {
			dispatch({
				type: 'SET_AUTORENEWAL',
				payload: {
					contractId: product.contract_id,
					from: account,
					depositId: deposit.deposit_id,
					autoRenewal: Boolean(e.target.checked),
				}
			})
	};
	const endDate = DateTime.fromSeconds(deposit.timestamp).plus({ days: product.days });
	const payoutDate = DateTime.fromSeconds(deposit.timestamp).plus({ days: product.payout_interval }); // 3 days then lock, then + payout_interval + 3 days, then lock....

	// console.log('product', product);
	/// console.log('deposit', deposit);

	return (
		<>
			<WithdrawalDialog
				open={Boolean(withdrawDialogDepositId)}
				deposit={deposit}
				product={product}
				onClose={() => setWithdrawDialogDepositId(null)}
			/>
			<TableRow key={deposit.transactionHash}>
				{/*<TableCell size="small">*/}
				{/*	{deposit.deposit_id}*/}
				{/*</TableCell>*/}
				<TableCell size="small">
					{MODE === 'development' && deposit.deposit_id} <EtherscanLink hash={deposit.transactionHash} type={'tx'} />
				</TableCell>
				<TableCell size="small" align="right">
					{DateTime.fromSeconds(deposit.timestamp).toFormat('dd.MM.yyyy')}
				</TableCell>
				<TableCell size="small" align="right">{endDate.toFormat('dd.MM.yyyy')}</TableCell>
				<TableCell size="small" align="right">{payoutDate.toFormat('dd.MM.yyyy')}</TableCell>
				{product.days > 1 && (<TableCell size="small">
					<Switch
						checked={deposit.autoRenewal}
						onChange={handleChangeAutoRenewal}
						disabled={depositAutoRenewalLoading}
						name="deposit-checkbox"
						color="primary"
					/>
					{depositAutoRenewalLoading && <CircularProgress size={20} />}
				</TableCell>
				)}
				<TableCell size="small" align="right" style={{ whiteSpace: 'nowrap' }}>
					{numeral(deposit.value).format('0.000000') !== 'NaN'
						? numeral(deposit.value).format('0.[000000]')
						: 0}{' '}
					{product.token_product_symbol}
					<Button
						variant="outlined"
						color="primary"
						size="small"
						disabled={isLoadingWithdrawal}
						onClick={() => setWithdrawDialogDepositId(`${deposit.product_id}.${deposit.deposit_id}`)}
						style={{ marginLeft: 8 }}
					>
						<FormattedMessage id="DepositTableRow.withdrawBtn" defaultMessage="Withdraw" />{' '}
						{withdrawalTransaction && withdrawalTransaction.transactionHash && (
							<EtherscanLink hash={withdrawalTransaction.transactionHash}>{''}</EtherscanLink>
						)}{' '}
						{isLoadingWithdrawal && <CircularProgress size={20} style={{ marginLeft: 8 }} />}
					</Button>
				</TableCell>
				{['GEG', 'GRG'].indexOf(product.token_product_symbol) < 0 && (
					<TableCell size="small" align="right" style={{ whiteSpace: 'nowrap' }}>
						{numeral(deposit.interestToClaim).format('0.000000') !== 'NaN'
							? numeral(deposit.interestToClaim).format('0.[000000]')
							: 0}{' '}
						{product.token_product_symbol}
					</TableCell>
				)}

				<TableCell size="small" align="right" style={{ whiteSpace: 'nowrap' }}>

					{numeral(deposit.interestToClaimGeg).format('0.000000') !== 'NaN'
						? numeral(deposit.interestToClaimGeg).format('0.[000000]')
						: 0}{' '}
					{product.token_reward_symbol}
					<Button
						variant="outlined"
						color="primary"
						size="small"
						disabled={isLoadingClaim || payoutDate.diffNow('days').days > 0}
						// disabled={isLoadingClaim || deposit.claim === 0 || payoutDate.diffNow('days').days > 0}
						style={{ marginLeft: 8 }}
						onClick={() => handleClickAction('MAKE_CLAIM', deposit.deposit_id)}
					>
						<FormattedMessage id="DepositTableRow.claimBtn" defaultMessage="Claim" />{' '}
						{claimTransaction && claimTransaction.transactionHash && (
							<EtherscanLink hash={claimTransaction.transactionHash}>{''}</EtherscanLink>
						)}{' '}
						{isLoadingClaim && <CircularProgress size={20} style={{ marginLeft: 8 }} />}
					</Button>
				</TableCell>
			</TableRow>
		</>
	);
};

export default DepositTableRow;
