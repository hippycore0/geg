import React from 'react';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import FinanceCommonParams from './FinanceCommonParams';
import ProductTable from './ProductTable';
import Faq from '../../components/Faq';
import Box from '@material-ui/core/Box';
import NavIcons from '../../components/AppHeader/NavIcons';
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
	root: {
		paddingTop: theme.spacing(4),
	},
	box: {
		[theme.breakpoints.down('sm')]: {
			flexDirection: 'column',
		},
	},
	header: {
		marginBottom: theme.spacing(4),
		flexGrow: 1,
		textTransform: 'uppercase',
		'& > svg': { color: theme.palette.primary.main, position: 'relative', top: '3px' },
	},
}));

const Finance = () => {
	const classes = useStyles();
	return (
		<Container className={classes.root}>
			<Box display="flex" className={classes.box}>
				<Typography variant="h5" className={classes.header}>
					<NavIcons.bag /> <FormattedMessage id="Routes.finance" defaultMessage="My finance" />
				</Typography>
				<FinanceCommonParams />
			</Box>
			<ProductTable />
			<Faq tag="Finance" />
		</Container>
	);
};

export default Finance;
