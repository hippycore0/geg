import React from 'react';
import { useSelector } from 'react-redux';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Alert from '@material-ui/lab/Alert';
import ProductTableRow from './ProductTableRow';
import { ProductListSelector } from '../../redux/selectors';
import { makeStyles } from '@material-ui/core';
import Loader from '../../components/Loader';
import { FormattedMessage } from 'react-intl';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	root: { marginBottom: theme.spacing(4) },
}));
const ProductTable = () => {
	const classes = useStyles();
	const { products, resourcesIsLoading } = useSelector((state) => ({
		products: ProductListSelector(state, false).filter((product) => product.deposits.length > 0),
		resourcesIsLoading:
			[
				'FETCH_PRODUCTS',
				'FETCH_TOKENS',
				'GET_ACCOUNT',
				'GET_DEPOSIT',
				'GET_WITHDRAWAL',
				'FETCH_PRODUCTS_DEPOSIT',
			].filter((type) => state.loading.indexOf(type) >= 0).length > 0,
	}));
	// console.log('deposit products', products);

	if (!products.length && !resourcesIsLoading) {
		return (
			<div className={classes.root}>
				<Paper variant="outlined">
					<Alert severity="warning">
						<FormattedMessage id="ProductTable.notFound" defaultMessage="No deposits found" />
					</Alert>
				</Paper>
				<Box align="center" style={{ padding: 20}}>
					<Button
						color="primary"
						variant="contained"
						component={RouterLink}
						to="/"
					>
						<FormattedMessage id="ProductTable.notFoundBtn" defaultMessage="Deposit now" />
					</Button>
				</Box>
			</div>
		);
	}

	return (
		<div className={classes.root}>
			<TableContainer component={Paper}>
				{!products.length && resourcesIsLoading && <Loader />}
				{products.length > 0 && (
					<Table>
						<TableHead>
							<TableRow>
								<TableCell />
								<TableCell variant="head">
									<FormattedMessage id="ProductTable.cellName" defaultMessage="Name" />
								</TableCell>
								<TableCell variant="head" align="right">
									<FormattedMessage id="ProductTable.cellAPY" defaultMessage="Fixed APY" />
								</TableCell>
								<TableCell variant="head" align="right">
									<FormattedMessage
										id="ProductTable.cellDepositPeriod"
										defaultMessage="Deposit period"
									/>
								</TableCell>
								<TableCell variant="head" align="right">
									<FormattedMessage
										id="ProductTable.cellPayoutInterval"
										defaultMessage="Payout interval"
									/>
								</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{products.map((product) => (
								<ProductTableRow key={product.id} product={product} />
							))}
						</TableBody>
					</Table>
				)}
			</TableContainer>
		</div>
	);
};

export default ProductTable;
